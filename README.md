# SNA App

This project was generated with Ionic v3

## Basic configuration

node version - 10.15.3

npm version - 6.4.1

ionic version - 3.2.0

cordova version - 8.1.2

typescript version - 2.6.2 or above

After project checkout run `npm install`

## Development server - browser

Run `ionic serve` for a dev server. Navigate to `http://localhost:8100/`. The app will automatically reload if you change any of the source files.

If you face any cors' problem, start web browser with disable-security flag enabled.

## Android Build

Needs: Android SDK installed, and current target Android Platform - version 6.3.0 (API 26)

Run `ionic cordova build android` to build the project. Use the `--prod` flag for a production build. The build artifacts will be stored in the `platforms\android\build\outputs\apk` directory.

## iOS Build

Needs: macOSx, XCode, iPhone and Provisioning Profile file.

1 - Run `ionic cordova build ios` to build the project. Use the `--prod` flag for a production build. The build artifacts will be stored in the `platforms\ios` directory.

2 - Open the Xcode project `platforms\ios\sna.xcodeproj`.

3 - Run clean code `Product (menu) -> Clean`.

4 - Select you iPhone as a target device.

5 - Run product archive `Product (menu) -> Archive`.

6 - Select the app and click on Distribute.

7 - Save for Enterprise or Ad Hoc Deployment.

8 - Choose your Provisioning Profile file.

9 - Choose a location to save the .ipa file.

To emulate on Xcode 9 - Run ionic cordova run ios

To emulate on Xcode 10 - Run ionic cordova emulate ios -l -- --buildFlag="-UseModernBuildSystem=0" --target="iPhone-X, 12.2"

## Troubleshoot:

Error: Device type "com.apple.CoreSimulator.SimDeviceType.undefined" could not be found.

Solution: on root folder run `cd platforms/ios/cordova && npm install ios-sim@latest`
