import { JsonObject, JsonProperty } from "json2typescript";

@JsonObject
export class AnexosModel {

    @JsonProperty('id')
    id: string = undefined;

    @JsonProperty('url')
    url: string = undefined;

  @JsonProperty('nome')
  nome: string = undefined;

}
