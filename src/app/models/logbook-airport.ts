import { JsonObject, JsonProperty } from "json2typescript";

@JsonObject
export class LogbookAirportModel {

    @JsonProperty('id')
    id: number = undefined;

    @JsonProperty('name')
    name: string = undefined;

    @JsonProperty('icao')
    icao: string = undefined;



}
