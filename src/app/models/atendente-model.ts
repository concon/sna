import { JsonObject, JsonProperty } from "json2typescript";

@JsonObject
export class AtendenteModel {

    @JsonProperty('nome')
    nome: string = undefined;

}
