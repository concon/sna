import { JsonObject, JsonProperty } from "json2typescript";

@JsonObject
export class UserModel {
    
    @JsonProperty('id')
    id: number = undefined;

    @JsonProperty('cpf')
    cpf: string = undefined;

    @JsonProperty('anaccode')
    anaccode: string = undefined;

    @JsonProperty('companhia')
    companhia: string = undefined;

    @JsonProperty('email')
    email: string = undefined;

    @JsonProperty('foto')
    foto: string = undefined;

    @JsonProperty('nome')
    nome: string = undefined;
    
}
