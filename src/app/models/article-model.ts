import { JsonObject, JsonProperty } from "json2typescript";

@JsonObject
export class ArticleModel {

    @JsonProperty('id')
    id: string = undefined;

    @JsonProperty('category_title')
    category: string = undefined;

    @JsonProperty('created_date')
    createdDate: string = undefined;

    @JsonProperty('title')
    title: string = undefined;

    @JsonProperty('content')
    content: string = undefined;
}
