import { JsonObject, JsonProperty } from "json2typescript";
import { SpecialtyModel } from './specialty-model';

@JsonObject
export class SpecialtyScheduleModel {

    @JsonProperty('id')
    id: number = undefined;

    @JsonProperty('status')
    status: string = undefined;

    @JsonProperty('data')
    date = undefined;

    @JsonProperty('especialidade')
    specialty: SpecialtyModel = undefined;
}
