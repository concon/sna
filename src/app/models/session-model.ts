import { JsonObject, JsonProperty } from "json2typescript";
import { UserModel } from './user-model';

@JsonObject
export class SessionModel {

    @JsonProperty('token')
    token: string = undefined;

    @JsonProperty('user')
    user: UserModel = undefined;
}
