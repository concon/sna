import { JsonObject, JsonProperty } from "json2typescript";

@JsonObject
export class LogbookModel {

    @JsonProperty('id')
    id: number = undefined;

    @JsonProperty('aeronaveId')
    aeronaveId: string = undefined;

    @JsonProperty('aeronaveTipo')
    aeronaveTipo: string = undefined;

    @JsonProperty('aeroportoChegada')
    aeroportoChegada: string = undefined;

    @JsonProperty('aeroportoSaida')
    aeroportoSaida: string = undefined;

    @JsonProperty('atualInst')
    atualInst: string = undefined;

    @JsonProperty('chegadaLocal')
    chegadaLocal: string = undefined;

    @JsonProperty('chegadaUTC')
    chegadaUTC: string = undefined;

    @JsonProperty('data')
    data: string = undefined;

    @JsonProperty('day')
    day: string = undefined;

    @JsonProperty('day_ldg')
    dayLdg: string = undefined;

    @JsonProperty('horasNoturnas')
    horasNoturnas: string = undefined;

    @JsonProperty('horasTotal')
    horasTotal: string = undefined;

    @JsonProperty('night')
    night: string = undefined;

    @JsonProperty('night_ldg')
    nightLdg: string = undefined;

    @JsonProperty('offDuttyLocal')
    offDuttyLocal: string = undefined;

    @JsonProperty('offDuttyUTC')
    offDuttyUTC: string = undefined;

    @JsonProperty('onDuttyLocal')
    onDuttyLocal: string = undefined;

    @JsonProperty('onDuttyUTC')
    onDuttyUTC: string = undefined;

    @JsonProperty('pic')
    pic: string = undefined;

    @JsonProperty('pic_p1')
    picP1: string = undefined;

    @JsonProperty('remarks')
    remarks: string = undefined;

    @JsonProperty('saidaLocal')
    saidaLocal: string = undefined;

    @JsonProperty('saidaUTC')
    saidaUTC: string = undefined;

    @JsonProperty('sic')
    sic: string = undefined;

    @JsonProperty('sic_p2')
    sicP2: string = undefined;

    @JsonProperty('voo')
    voo: string = undefined;

    @JsonProperty('vooPiloto')
    vooPiloto: boolean = undefined;

}
