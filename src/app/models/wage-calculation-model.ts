import { JsonObject, JsonProperty } from "json2typescript";
import { WageCalculationUnitModel } from './wage-calculation-unit-model';

@JsonObject
export class WageCalculationModel {

    @JsonProperty('executada', WageCalculationUnitModel)
    executed: WageCalculationUnitModel = undefined;

    @JsonProperty('planejada', WageCalculationUnitModel)
    planned: WageCalculationUnitModel = undefined;
}
