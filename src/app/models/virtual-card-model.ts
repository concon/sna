import { JsonObject, JsonProperty } from "json2typescript";

@JsonObject
export class VirtualCardModel {

    @JsonProperty('matricula')
    id: number = undefined;

    @JsonProperty('nome')
    name: string = undefined;

    @JsonProperty('funcao')
    role: string = undefined;

    @JsonProperty('empresa')
    company: string = undefined;

    @JsonProperty('dtadmi')
    admissionDate: string = undefined;

    @JsonProperty('dtvalicart')
    validationDate: string = undefined;
}
