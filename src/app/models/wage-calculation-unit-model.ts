import { JsonObject, JsonProperty } from "json2typescript";

@JsonObject
export class WageCalculationUnitModel {

    @JsonProperty('id')
    id: number = undefined;

    @JsonProperty('adicionalPericulosidade')
    additionalHazard: number = undefined;

    @JsonProperty('compensacaoOrganica')
    organicCompensation: number = undefined;

    @JsonProperty('salarioBase')
    salarioBase: number = undefined;

    @JsonProperty('diarias')
    daily: string = undefined;

    @JsonProperty('file')
    file: string = undefined;

    @JsonProperty('horasCompensadas')
    compensatedHours: string = undefined;

    @JsonProperty('horasDiurnas')
    dayHours: string = undefined;

    @JsonProperty('horasEspeciais')
    specialHours: string = undefined;

    @JsonProperty('horasNoturnasEspeciais')
    horasNoturnasEspeciais: string = undefined;

    @JsonProperty('horasNoturnas')
    nightHours: string = undefined;

    @JsonProperty('horasRemuneradas')
    paidHours: string = undefined;

    @JsonProperty('horasVoadas')
    flownHours: string = undefined;

    @JsonProperty('salarioLiquido')
    netSalary: string = undefined;

    @JsonProperty('mes')
    month: string = undefined;

    @JsonProperty('ano')
    year: string = undefined;

    @JsonProperty('data')
    data: string = undefined;

    @JsonProperty('salario')
    wage: number = undefined;

    @JsonProperty('tipo')
    type: string = undefined;

    @JsonProperty('valorDiarias')
    valorDiarias: string = undefined;

    @JsonProperty('horasVoadas')
    horasVoadas: string = undefined;

    @JsonProperty('horasReserva')
    horasReserva: string = undefined;

    @JsonProperty('horasReservaNoturna')
    horasReservaNoturna: string = undefined;

    @JsonProperty('horasSobreaviso')
    horasSobreaviso: string = undefined;

    @JsonProperty('horasAdicionalSDU')
    horasAdicionalSDU: string = undefined;

    @JsonProperty('horasCat1')
    horasCat1: string = undefined;

    @JsonProperty('horasCat1Noturna')
    horasCat1Noturna: string = undefined;

    @JsonProperty('desSNA')
    desSNA: string = undefined;

    @JsonProperty('horasDomFevDiurno')
    horasDomFevDiurno: string = undefined;

    @JsonProperty('horasDomFevNoturno')
    horasDomFevNoturno: string = undefined;

    @JsonProperty('valorHorasDomFevDiurno')
    valorHorasDomFevDiurno: string = undefined;

    @JsonProperty('valorHorasDomFevNoturno')
    valorHorasDomFevNoturno: string = undefined;

    @JsonProperty('inss')
    inss: string = undefined;

    @JsonProperty('irrf')
    irrf: string = undefined;

    @JsonProperty('horasNoturnasNormal')
    horasNoturnasNormal: string = undefined;

    @JsonProperty('horasTotais')
    horasTotais: string = undefined;

    @JsonProperty('totalProventos')
    totalProventos: string = undefined;

    @JsonProperty('totalDescontos')
    totalDescontos: string = undefined;

    @JsonProperty('fgts')
    fgts: string = undefined;

    @JsonProperty('valorhorasVoo')
    valorHorasVoo: string = undefined;

    @JsonProperty('horasVoo')
    horasVoo: string = undefined;

    @JsonProperty('totalHorasNoturnasNormal')
    totalHorasNoturnasNormal: string = undefined;

    @JsonProperty('contagemSDU')
    contagemSDU: string = undefined;

    @JsonProperty('horasCat1Diurnas')
    horasCat1Diurnas: string = undefined;

    @JsonProperty('horasDiurnasEspeciais')
    horasDiurnasEspeciais: string = undefined;

    @JsonProperty('horasDiarias10')
    horasDiarias10: string = undefined;

    @JsonProperty('horasDiarias25')
    horasDiarias25: string = undefined;
}
