import { JsonObject, JsonProperty } from "json2typescript";
import { AtendenteModel } from "./atendente-model";

@JsonObject
export class DetalhesAtendimentoModel {

    @JsonProperty('atendente')
    atendente: AtendenteModel = undefined;

}
