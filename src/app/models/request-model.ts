import { JsonObject, JsonProperty } from "json2typescript";
import { ChatModel } from "./chat-model";
import { AnexosModel } from "./anexos-model";

@JsonObject
export class RequestModel {

    @JsonProperty('id')
    id: string = undefined;

    @JsonProperty('status')
    status: string = undefined;

    @JsonProperty('tipo')
    type: string = undefined;

    @JsonProperty('sumario')
    summary: string = undefined;

    @JsonProperty('date', [Number], true)
    date: [Number] = undefined;

    @JsonProperty('chats', [ChatModel], true)
    chats: [ChatModel] = undefined;

    @JsonProperty('anexos', [AnexosModel], true)
    anexos: [AnexosModel] = undefined;

}
