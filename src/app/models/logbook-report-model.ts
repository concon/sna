import { JsonObject, JsonProperty } from "json2typescript";

@JsonObject
export class LogbookReportModel {

    @JsonProperty('url')
    url: string = undefined;
}
