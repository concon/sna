import { JsonObject, JsonProperty } from "json2typescript";

@JsonObject
export class CargoModel {

    @JsonProperty('id')
    id: number = undefined;

    @JsonProperty('nome')
    name: string = undefined;
}
