import { JsonObject, JsonProperty } from "json2typescript";

@JsonObject
export class TimeModel {

    @JsonProperty('hour')
    hour: number = undefined;

    @JsonProperty('minute')
    minute: number = undefined;

    @JsonProperty('nano')
    nano: number = undefined;

    @JsonProperty('second')
    second: number = undefined;
}
