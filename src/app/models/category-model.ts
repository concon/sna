import { JsonObject, JsonProperty } from "json2typescript";

@JsonObject
export class CategoryModel {

    @JsonProperty('id')
    id: string = undefined;

    @JsonProperty('title')
    title: string = undefined;

    @JsonProperty('description')
    description: string = undefined;

    @JsonProperty('parent_id')
    parentId: string = undefined;

    @JsonProperty('numitems')
    numItems: string = undefined;
}
