import { JsonObject, JsonProperty } from "json2typescript";
import { DetalhesAtendimentoModel } from "./detalhes-atendimento-model";

@JsonObject
export class ChatModel {

    @JsonProperty('id')
    id: string = undefined;

    @JsonProperty('time')
    time: string = undefined;

    @JsonProperty('texto')
    text: string = undefined;

    @JsonProperty('atendimento', DetalhesAtendimentoModel, true)
    atendimento: DetalhesAtendimentoModel = undefined;


}
