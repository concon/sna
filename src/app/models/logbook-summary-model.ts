import { JsonObject, JsonProperty } from "json2typescript";

@JsonObject
export class Currency {

    @JsonProperty('day')
    day: number = undefined;

    @JsonProperty('instrument')
    instrument: number = undefined;

    @JsonProperty('night')
    night: number = undefined;
}

@JsonObject
export class Entries {

    @JsonProperty('all')
    all: string = undefined;

    @JsonProperty('seven_days')
    sevenDays: string = undefined;

    @JsonProperty('one_month')
    oneMonth: string = undefined;

    @JsonProperty('six_month')
    sixMonth: string = undefined;

    @JsonProperty('year')
    year: string = undefined;
}

@JsonObject
export class Limits {

    @JsonProperty('month')
    month: string = undefined;

    @JsonProperty('thirty_in_seven')
    thirtyInSeven: string = undefined;

    @JsonProperty('tree_month')
    threeMonth: string = undefined;

    @JsonProperty('twelve_months')
    twelveMonths: string = undefined;

    @JsonProperty('twenty_eight_days')
    twentyEightDays: string = undefined;

    @JsonProperty('year')
    year: string = undefined;
}

@JsonObject
export class SmartList {

    @JsonProperty('companhia')
    company: string = undefined;

    @JsonProperty('time')
    time: string = undefined;
}


@JsonObject
export class LogbookSummaryModel {

    @JsonProperty('currency', Currency)
    currency: Currency = undefined;

    @JsonProperty('entries', Entries)
    entries: Entries = undefined;

    @JsonProperty('limits', Limits)
    limits: Limits = undefined;

    @JsonProperty('smartList', [SmartList])
    smartList: SmartList = undefined;
}