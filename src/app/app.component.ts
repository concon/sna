import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthGuardProvider } from '../providers/auth-guard/auth-guard'
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

import { FCM } from '@ionic-native/fcm/ngx';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;

  //rootPage:any = 'HomePage';

  constructor(private platform: Platform,
              private statusBar: StatusBar,
              private splashScreen: SplashScreen,
              private events: Events,
              private authGuardProvider: AuthGuardProvider,
              private inAppBrowser: InAppBrowser,
              private screenOrientation: ScreenOrientation,
              private fcm: FCM) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      if (platform.is('android')) {
        this.statusBar.backgroundColorByHexString('#ffffff');
      }
      this.splashScreen.hide();

      this.nav.setRoot('HomePage');

      this.subscribeEvents();

      if ((this.platform.is('android') || this.platform.is('ios') || this.platform.is('core')) && !this.platform.is('mobileweb')) {
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);

        this.fcm.subscribeToTopic('all');

        this.fcm.onNotification().subscribe(data => {
          if(data.wasTapped){
            console.log("Received in background");
          } else {
            console.log("Received in foreground");
          };
        });
      }

    });
  }

  private subscribeEvents() {
    this.pageOpenEventSubscribe();
    this.inappOpenEventSubscribe();
    this.pdfOpenEventSubscribe();
    this.pageBackEventSubscribe();
    this.backToRootEventSubscribe();
    this.closePageEventSubscribe();
  }

  private pageOpenEventSubscribe() {
    this.events.subscribe('page:open', (page, params) => {
      if (page == "LoginPage") {
        this.nav.push('LoginPage', { targetType: "page", targetPage: page, targetParams: params });
      } else {
        this.authGuardProvider.validateAccess(page).then((isValidAccess) => {
          if (isValidAccess) {
            this.authGuardProvider.validateProfileData(page).then((isValidProfileData) => {
              if (isValidProfileData) {
                this.nav.push(page, params);
              } else {
                this.nav.push('UpdateProfileDataPage', { targetType: "page", targetPage: page, targetParams: params });
              }
            });
          } else {
            this.nav.push('LoginPage', { targetType: "page", targetPage: page, targetParams: params });
          }
        });
      }
    });
  }

  private inappOpenEventSubscribe() {
    this.events.subscribe('inapp:open', (page, params) => {
      this.authGuardProvider.validateAccess(page).then((isValidAccess) => {
        if (isValidAccess) {
          this.authGuardProvider.validateProfileData(page).then((isValidProfileData) => {
            if (isValidProfileData) {
              this.openInAppBrowser(params);
            } else {
              this.nav.push('UpdateProfileDataPage', { targetType: "inapp", targetParams: params });
            }
          });
        } else {
          this.nav.push('LoginPage', { targetType: "inapp", targetParams: params });
        }
      });
    });
  }

  private pdfOpenEventSubscribe() {
    this.events.subscribe('pdf:open', (page, params) => {
      this.authGuardProvider.validateAccess(page).then((isValidAccess) => {
        if (isValidAccess) {
          this.authGuardProvider.validateProfileData(page).then((isValidProfileData) => {
            if (isValidProfileData) {
              this.openPDF(params);
            } else {
              this.nav.push('UpdateProfileDataPage', { targetType: "pdf", targetParams: params });
            }
          });
        } else {
          this.nav.push('LoginPage', { targetType: "pdf", targetParams: params });
        }
      });
    });
  }

  private pageBackEventSubscribe() {
    this.events.subscribe('page:back', () => {
      this.nav.pop();
    });
  }

  private backToRootEventSubscribe() {
    this.events.subscribe('page:backToRoot', () => {
      this.nav.popToRoot();
    });
  }

  private closePageEventSubscribe() {
    this.events.subscribe('page:closePage', (page) => {
      let pageIndex = this.findIndexViewById(this.nav.getViews(), page);
      if (pageIndex > -1) {
        this.nav.remove(pageIndex)
      }
    });
  }

  private openPDF(params) {
    window.open(params.targetUrl,'_system', 'location=yes');
  }

  public findIndexViewById(viewList, page) {
    var foundIndex = -1;
    viewList.forEach((view, index) => {
      if (view.id == page) {
        foundIndex = index;
      }
    });
    return foundIndex;
  }

  private openInAppBrowser(params) {
    const options: InAppBrowserOptions = {
      zoom: 'yes',
      toolbar: 'yes',
      toolbarposition: 'top',
      toolbarcolor: '#ffffff',
      enableViewportScale: 'yes',
      closebuttoncaption: 'Voltar ao SNA',
      closebuttoncolor: '#000000',
      toolbartranslucent: 'no',
      hidenavigationbuttons: 'yes',
      hideurlbar: 'yes',
      location: 'yes',
      clearcache: 'yes',
      shouldPauseOnSuspend: 'yes',
      transitionstyle: 'coververtical'
    };

    var browser = this.inAppBrowser.create(params.targetUrl, '_blank', options);

    browser.on("loaderror").subscribe(() => {
      browser.close();
      alert("Não foi possível processar sua requisição, tente novamente.");
    });

    if (!(this.platform.is('mobileweb') || this.platform.is('android'))) {
      browser.on("loadstop").subscribe(() => {
        browser.insertCSS({ code: "html {margin-top: 44px;" });
      });
    }

    browser.show()
  }

}
