import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, LOCALE_ID } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HTTP } from '@ionic-native/http';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { IonicStorageModule } from '@ionic/storage';
import { MomentModule } from 'ngx-moment';
import { DatePipe } from '@angular/common';

import { MyApp } from './app.component';
import { ApiProvider } from '../providers/api/api';
import { AuthGuardProvider } from '../providers/auth-guard/auth-guard';
import { HttpNativeProvider } from '../providers/http-native/http-native';
import { HttpAngularProvider } from '../providers/http-angular/http-angular';
import { HttpProvider } from '../providers/http/http';
import { SessionProvider } from '../providers/session/session';
import { Network } from '@ionic-native/network';
import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

import localePtBr from '@angular/common/locales/pt';
import { registerLocaleData } from '@angular/common';
import { FCM } from '@ionic-native/fcm/ngx';

registerLocaleData(localePtBr);

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      statusbarPadding: false,
      backButtonText: ''
    }),
    HttpClientModule,
    MomentModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    DatePipe,
    StatusBar,
    SplashScreen,
    Network,
    SessionProvider,
    File,
    Transfer,
    TransferObject,
    FileTransfer,
    FilePath,
    Camera,
    HTTP,
    HttpClient,
    HttpClientModule,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {provide: LOCALE_ID, useValue: "pt-br"},
    ApiProvider,
    AuthGuardProvider,
    HttpNativeProvider,
    HttpAngularProvider,
    HttpProvider,
    InAppBrowser,
    SocialSharing,
    ScreenOrientation,
    FCM
  ]
})
export class AppModule {}
