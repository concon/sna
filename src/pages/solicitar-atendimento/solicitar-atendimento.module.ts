import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SolicitarAtendimentoPage } from './solicitar-atendimento';

@NgModule({
  declarations: [
    SolicitarAtendimentoPage,
  ],
  imports: [
    IonicPageModule.forChild(SolicitarAtendimentoPage),
  ],
})
export class SolicitarAtendimentoPageModule {}
