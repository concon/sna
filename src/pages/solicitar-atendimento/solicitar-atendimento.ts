import { Component, ViewChild, ElementRef } from '@angular/core';
import { Platform, IonicPage, LoadingController, Events, NavParams } from 'ionic-angular';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiProvider } from '../../providers/api/api';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/first';

import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';

declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-solicitar-atendimento',
  templateUrl: 'solicitar-atendimento.html',
})
export class SolicitarAtendimentoPage {

  @ViewChild('fileInp') fileInput: ElementRef;
	private loading;
  public requestForm: FormGroup;
  public hasFormError = false;
  public tipo;
  public anonimoAtivo;

  private selectedFiles = [];

  private isNativePlatform = false;

  constructor(public platform: Platform,
              public loadingCtrl: LoadingController,
              public filePath: FilePath,
              public file: File,
  						public events: Events,
  						public api: ApiProvider,
              public navParams: NavParams) {
  	this.requestForm = new FormGroup({
      tipo: new FormControl('', [Validators.required]),
      texto: new FormControl('', [Validators.required]),
      assunto: new FormControl('', [Validators.required]),
      atendente: new FormControl(0, [Validators.required]),
      resposta: new FormControl('', []),
      status: new FormControl('ABERTO', [Validators.required])
    });

    let requestType = this.navParams.get('tipoAtendimento')
    this.requestForm.controls['tipo'].setValue(requestType);

    if (requestType == "DENUNCIA") {
      this.anonimoAtivo = true;
      this.tipo = "Denúncia";
    } else if (requestType == "RELPREV") {
      this.tipo = "Relprev";
    } else if (requestType == "JURIDICO") {
      this.tipo = "Jurídico";
    }
  }

  ngAfterViewInit() {
    this.platform.ready().then(() => {
        this.isNativePlatform = !(this.platform.is('mobileweb') || this.platform.is('android'));
    });
  }

  public doRequest() {
  	this.hasFormError = !this.requestForm.valid;
    if (this.requestForm.valid) {
      this.showLoading();
    	this.api.doNewRequest(this.requestForm.value).subscribe(
        res => {
        	if (this.selectedFiles.length > 0) {
        		this.orchestrateFilesUploading(res.id);
        	} else {
        		alert("Solicitação registrada com sucesso!");
        		this.closePage();
        		this.hideLoading();
        	}
        }, message => {
          alert(message);
          this.hideLoading();
        }
      );
    }
  }

  // iOS only, remove a created copy of file from temporary folder
  public removeLocalFile(others) {
    if (this.isNativePlatform && others && others[0] && others[0].fileName) {
      this.file.removeFile(cordova.file.dataDirectory, others[0].fileName).catch(err => console.log(err));
    }
  }

  // Orchestrate files uploading, using a FIFO concept
  // Stock all file errors and show at the end of process
  private async orchestrateFilesUploading(requestId) {
    let errorFiles = [];

    for(let selectedFile of this.selectedFiles) {
      var preparedFile = await this.prepareUploadingFile(selectedFile);
      try {
        await this.doUploadFile(requestId, preparedFile[0], preparedFile[1], selectedFile.name);
      } catch(errorMessage) {
        errorFiles.push(selectedFile.name);
      }
      this.removeLocalFile(preparedFile[1]);
    }

    if (errorFiles.length > 0) {
      alert("Solicitação registrada com sucesso, porém ocorreu algum erro no upload do(s) arquivo(s): " + errorFiles.join(", ") + ". Tente adicioná-lo(s) mais tarde.");
    } else {
      alert("Solicitação registrada com sucesso!");
    }
    this.closePage();
    this.hideLoading();
  }

  // Return a promise to synchrounous file upload
  async doUploadFile(requestId, fileFormData, others, nome): Promise<any> {
   return new Observable((observer) => {
    	this.api.doNewRequestUploadFile(requestId, fileFormData, others, nome).subscribe(
        res => {
        	observer.next();
        }, message => {
          observer.error();
        }
      );
    }).first().toPromise();
  }

  // Remove file from array
  public removeFile(id) {
    this.selectedFiles.splice(id, 1);
  }

  // Add new file to array
  public fileChange(event) {
    let files = event.target.files;

    for(let file of files) {
      this.selectedFiles.push(file);
    }
  }

  // Call native file manager
  public addFileNextStep() {
    this.fileInput.nativeElement.click();
  }

  // Prepare file to upload based on working rule of Android (webFormData) and iOS (NativePlatform)
  private async prepareUploadingFile(file) {
    var webFormData:FormData = new FormData();
    var nativeFormData = [];

    if (this.isNativePlatform) {
      await this.file.writeFile(cordova.file.dataDirectory, file.name, file, { replace: true }).then((fileEntry) => {
        nativeFormData.push({
          fileKey: "file",
          fileName: file.name,
          chunkedMode: false,
          mimeType: "multipart/form-data",
          params : {'file': file.name}
        });
        nativeFormData.push(cordova.file.dataDirectory + file.name);
      }).catch((err) => {
        alert("Não foi possível anexar o arquivo.");
      });
    } else {
      webFormData.append('file', file);
    }

    return [webFormData, nativeFormData];
  }

  public closePage() {
    this.events.publish('page:back');
  }

  public showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Registrando solicitação...'
    });
    this.loading.present();
  }

  public hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

}
