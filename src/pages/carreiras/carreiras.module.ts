import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CarreirasPage } from './carreiras';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    CarreirasPage,
  ],
  imports: [
    IonicPageModule.forChild(CarreirasPage),
    ComponentsModule
  ]
})
export class CarreirasPageModule {}
