import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CalculoJornadaPage } from './calculo-jornada';

@NgModule({
  declarations: [
    CalculoJornadaPage,
  ],
  imports: [
    IonicPageModule.forChild(CalculoJornadaPage),
  ],
})
export class CalculoJornadaPageModule {}
