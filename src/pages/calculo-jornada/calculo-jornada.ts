import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-calculo-jornada',
  templateUrl: 'calculo-jornada.html',
})
export class CalculoJornadaPage {
  public jornadas = "";

  constructor() {
    this.onChange("jornada_p00");
  }

  onChange(value){
    this.jornadas = value;
  }

}
