import { Component } from '@angular/core';
import { IonicPage, Platform, NavParams, ToastController, Events, AlertController, LoadingController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html',
})
export class CadastroPage {

  public registrationForm: FormGroup;
  public passwordConfirmation = "";
  public hasFormError = false;
  public isPasswordDifferent = false;
  public storePersonalDataAgreement = false;
  public loginCallback;
  public loading;

  constructor(public platform: Platform,
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              public events: Events,
              public api: ApiProvider,
              public toastCtrl: ToastController,
              public forgotCtrl: AlertController) {
    //let CPFPATTERN = /^[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}*$/;
    this.loginCallback = this.navParams.get("loginCallback");

    this.registrationForm = new FormGroup({
      cpf: new FormControl('', [Validators.required, Validators.minLength(13)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      senha: new FormControl('', [Validators.required, Validators.minLength(1)])
    });
  }

  public doRegistration() {
    this.hasFormError = !this.registrationForm.valid;
    this.isPasswordDifferent = this.registrationForm.value.senha != this.passwordConfirmation;

    if (this.registrationForm.valid && !this.isPasswordDifferent) {
      this.showLoading();
      this.api.doUserRegistration(this.registrationForm.value).subscribe(
        sessionData => {
          this.loginCallback(sessionData).then(() => {
            this.hideLoading();

            this.closeCadastroModal();
          });
        }, message => {
          alert(message);
          this.hideLoading();
        }
      );
    }
  }

  public goToTermsAndConditions() {
    this.events.publish('pdf:open', 'PublicFile', { targetUrl: "https://s3.amazonaws.com/snaaplicativo/politica/1548271877_3oEcZgS97k_termos_e_condicoes_gerais_de_uso_e_ou_de_venda+(1).pdf" });
  }

  public closeCadastroModal() {
    this.events.publish('page:closePage', "CadastroPage");
  }

  public showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Efetuando o cadastro...'
    });

    this.loading.present();
  }

  public goToCadastro() {
    this.events.publish('page:open', 'CadastroPage', null);
  }

  public hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

}
