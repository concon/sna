import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewDetailsPage } from './new-details';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    NewDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(NewDetailsPage),
    PipesModule
  ],
})
export class NewDetailsPageModule {}
