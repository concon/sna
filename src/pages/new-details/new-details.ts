import { Component } from '@angular/core';
import { IonicPage, NavParams, LoadingController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { SocialSharing } from '@ionic-native/social-sharing';

@IonicPage()
@Component({
  selector: 'page-new-details',
  templateUrl: 'new-details.html',
})
export class NewDetailsPage {

  public id;
  public description;
  public category;
  public title;
  private url;
  private hasSocialShare;
  public enableSocialShare = false;

  public loading;

  constructor(public navParams: NavParams,
              public api: ApiProvider,
              public loadingCtrl: LoadingController,
              public socialSharing: SocialSharing) {
    this.id = this.navParams.get('id');
    this.hasSocialShare = this.navParams.get('hasSocialShare');

    if (this.navParams.get('shouldLoadData')) {
      this.loading = this.loadingCtrl.create({
        content: 'Carregando...'
      });
      this.loadNewById();
    } else {
      this.description = this.navParams.get('description');
      this.title = this.navParams.get('title');
      this.category = this.navParams.get('category');
      this.url = this.navParams.get('url');
      this.validateSocialShare();
    }
  }

  private loadNewById() {
    this.loading.present();
    this.api.getNewById(this.id).subscribe(
      articleModel => {
        this.description = articleModel.content;
        this.category = articleModel.category;
        this.title = articleModel.title;
        this.url = "https://www.aeronautas.org.br/" + articleModel.id;
        this.validateSocialShare();
        this.loading.dismiss();
      }, errorMessage => {
        alert(errorMessage);
        this.loading.dismiss();
      }
    );
  }

  private validateSocialShare() {
    if (this.hasSocialShare) {
      this.enableSocialShare = true;
    }
  }

  public shareTo() {
    alert(this.url);
    var options = {
      message: this.title,
      subject: 'SNA - Compartilhamento',
      url: this.url
    };

    this.socialSharing.shareWithOptions(options);
  }

}
