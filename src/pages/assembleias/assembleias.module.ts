import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AssembleiasPage } from './assembleias';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    AssembleiasPage,
  ],
  imports: [
    IonicPageModule.forChild(AssembleiasPage),
    ComponentsModule
  ]
})
export class AssembleiasPageModule {}
