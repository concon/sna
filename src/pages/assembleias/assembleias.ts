import { Component } from '@angular/core';
import { Platform, IonicPage } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { ArticleModel } from '../../app/models/article-model';

@IonicPage()
@Component({
  selector: 'page-assembleias',
  templateUrl: 'assembleias.html',
})
export class AssembleiasPage {

  private offset = 0;
  private offsetValue = 5;
  public isLoadingNews = false;
  public articleList: ArticleModel[];

  constructor(public platform: Platform,
              public api: ApiProvider) {
    this.initScreenPlaceHolder();
  }

  ngAfterViewInit() {
    this.platform.ready().then(() => {
       this.loadLatestNews(null, 10);
    });
  }

  private initScreenPlaceHolder() {
    this.articleList = [new ArticleModel(), new ArticleModel(), new ArticleModel()];
  }

  public loadLatestNews(infiniteScroll, quantity) {
    this.api.getAssembliesWithOffset(this.offset, quantity).subscribe(
      articles => {
        if (this.offset == 0) {
          this.articleList = articles;
          this.offset = quantity;
        } else {
          this.linkList(articles);
        }

        if (infiniteScroll) infiniteScroll.complete();
        this.isLoadingNews = false;
      }, errorMessage => {
        if (infiniteScroll) infiniteScroll.complete();
        this.isLoadingNews = false;
      }
    );
  }

  private linkList(receivedArticleList) {
    this.articleList = this.articleList.concat(receivedArticleList);
  }

  public doInfinite(infiniteScroll) {
    if (!this.isLoadingNews) {
      this.isLoadingNews = true;

      this.offset += this.offsetValue;
      this.loadLatestNews(infiniteScroll, 5);
    }
  }

}
