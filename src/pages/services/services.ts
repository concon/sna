import { Component } from '@angular/core';
import { IonicPage, NavParams, Events } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-services',
  templateUrl: 'services.html',
})
export class ServicesPage {

  constructor(public navParams: NavParams,
              private events: Events) { }

  // swipeEvent(e) {
  //     if (e.direction == 1) {
  //       this.events.publish('page:back');
  //     }
  // }

  public goToNewsPage(event) {
    this.events.publish('page:open', 'NewsPage', null);
  }

  public goToServicesPage(event) {
    this.events.publish('page:open', 'ServicesPage', null);
  }

  public goToParceriasPage(event) {
    this.events.publish('page:open', 'ParceriasPage', null);
  }

  public goToCarreirasPage(event) {
    this.events.publish('page:open', 'CarreirasPage', null);
  }

  public goToAssembleiasPage(event) {
    this.events.publish('page:open', 'AssembleiasPage', null);
  }

  public goToAcoesColetivasPage(event) {
    this.events.publish('page:open', 'AcoesColetivasPage', null);
  }

  public goToCarteirinhaExternalPage(event) {
    this.events.publish('page:open', 'CarteirinhaVirtualPage', null);
  }

  public goToEscritoriosRegionais(event) {
    this.events.publish('page:open', 'NewDetailsPage', { id: 795, shouldLoadData: true });
  }

  public goToPasseLivrePage(event) {
    this.events.publish('page:open', 'PasseLivrePage', null);
  }

  public goToSolicitarAtendimentoPage(event, tipoAtendimento) {
    this.events.publish('page:open', 'ListaSolicitacoesPage', { tipoAtendimento: tipoAtendimento });
  }

  public goToAgendarConsultaPage(event) {
    this.events.publish('page:open', 'AgendarConsultaPage', null);
  }

  public goToCalculoJornadaPage(event) {
    this.events.publish('page:open', 'CalculoJornadaPage', null);
  }

  public goToListaCalculoSalarioPage(event) {
    this.events.publish('page:open', 'ListaSalarioPage', null);
  }

  public goToLogBookListaPage(event) {
    this.events.publish('page:open', 'LogBookListaPage', null);
  }


}
