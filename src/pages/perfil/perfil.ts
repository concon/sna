import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, Events, NavParams, LoadingController, Platform, AlertController, ActionSheetController, normalizeURL } from 'ionic-angular';
import { SessionProvider } from '../../providers/session/session';
import { ApiProvider } from '../../providers/api/api';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserModel } from '../../app/models/user-model';

import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';

declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-perfil',
  templateUrl: 'perfil.html',
})
export class PerfilPage {

  @ViewChild('fileInp') fileInput: ElementRef;
  public openPassword = false;
  public loading;
  public userModel: UserModel = new UserModel();
  public passwordChangeForm: FormGroup;
  public imageChangeForm: FormGroup;
  public passwordConfirmation = "";
  public intendedPhotoSrc: string | ArrayBuffer = "";
  public lastImage: string = null;

  public hasFormError = false;
  public isPasswordDifferent = false;
  public isChangingImage = false;

  public labelFile = "Selecionar foto...";

  public fileFormData:FormData = new FormData();
  public others = [];

  private isNativePlatform = false;

  constructor(public platform: Platform,
              public navParams: NavParams,
              public logoutCtrl: AlertController,
              public imageCtrl: AlertController,
              public events: Events,
              public api: ApiProvider,
              private camera: Camera,
              private file: File,
              private filePath: FilePath,
              public loadingCtrl: LoadingController,
              public actionSheetCtrl: ActionSheetController,
              public session: SessionProvider) {
    this.imageChangeForm = new FormGroup({
      file: new FormControl('', [Validators.required])
    });

    this.passwordChangeForm = new FormGroup({
      senhaAtual: new FormControl('', [Validators.required, Validators.minLength(1)]),
      senha: new FormControl('', [Validators.required, Validators.minLength(1)])
    });
  }

  ngAfterViewInit() {
    this.platform.ready().then(() => {
        this.isNativePlatform = !(this.platform.is('mobileweb') || this.platform.is('android'));
        this.loadUserInfo();
    });
  }

  ionViewWillLeave() {
    //this.hideLoading();
  }

  public goOpenPassword() {
    this.openPassword = !this.openPassword;
  }

  public goLogout() {
    this.logoutCtrl.create({
      title: 'Logout',
      message: "Você deseja realmente sair?",
      buttons: [
        {
          text: 'Sim',
          handler: data => {
            this.session.remove();
            this.events.publish('page:back');
          }
        },
        {
          text: 'Não',
          handler: data => { }
        }
      ]
    }).present();
  }

  public loadUserInfo() {
    this.showLoading("Carregando dados...");
    this.api.getUserInfo().subscribe(
      userModel => {
        this.userModel = userModel;
        this.updateUserDataSession(userModel);
        this.hideLoading();
      }, error => {
        alert(error);
        this.hideLoading();
      }
    );
  }

  public updateUserDataSession(userModel) {
    return this.session.updateUserData(userModel);
  }

  public showLoading(message) {
    this.loading = this.loadingCtrl.create({
      content: message
    });

    this.loading.present();
  }

  public hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  public saveChanges() {
    this.hasFormError = !this.passwordChangeForm.valid;
    this.isPasswordDifferent = this.passwordChangeForm.value.senha != this.passwordConfirmation;

    if (this.passwordChangeForm.valid && !this.isPasswordDifferent) {
      this.showLoading("Salvando dados...");
      this.api.doChangePassword(this.passwordChangeForm.value).subscribe(
        res => {
          alert("Troca de senha efetuada com sucesso.");
          this.openPassword = false;
          this.passwordChangeForm.reset();
          this.passwordConfirmation = "";
          this.hideLoading();
        }, error => {
          alert(error.message);
          this.hideLoading();
        }
      );
    }
  }

  public doUploadFile() {
    this.showLoading("Salvando dados...");
    this.api.doChangeImage(this.fileFormData, this.others).subscribe(
      res => {
        alert("Imagem atualizada com sucesso!");
        this.hideLoading();
        this.loadUserInfo();
        this.cancelUploadFile();
      }, message => {
        alert(message);
        this.hideLoading();
        this.cancelUploadFile();
      }
    );
  }

  public removeLocalFile() {
    if (this.isNativePlatform && this.others && this.others[0] && this.others[0].fileName) {
      this.file.removeFile(cordova.file.dataDirectory, this.others[0].fileName).catch(err => console.log(err));
    }
  }

  public cancelUploadFile() {
    this.labelFile = "Selecionar foto...";
    this.intendedPhotoSrc = "";
    this.isChangingImage = false;
    this.resetImage();
  }

  public resetImage() {
    this.removeLocalFile();
    this.fileFormData = new FormData();
    this.others = [];
  }

  public addFile() {
    this.fileInput.nativeElement.click();
  }

  public openChangeImageModal() {
    this.isChangingImage = true;
  }

  public fileChange(event) {
    let fileList: FileList = event.target.files;

    if (fileList.length > 0) {
        let file = fileList[0];
        this.readURL(event);
        this.fileFormData.append('file', file);
        this.labelFile = "Alterar";
      }
  }

  public addFileNextStep() {
    this.resetImage();

    if (this.isNativePlatform) {
      this.presentActionSheet();
    } else {
      this.addFile();
    }
  }

  private readURL(event): void {
    if (event.target.files && event.target.files[0]) {
        const file = event.target.files[0];

        const reader = new FileReader();

        reader.onload = e => this.intendedPhotoSrc = reader.result;

        reader.readAsDataURL(file);
    }
  }


  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Selecione a fonte da imagem:',
      buttons: [
        {
          text: 'Galeria',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        // {
        //   text: 'Câmera',
        //   handler: () => {
        //     this.takePicture(this.camera.PictureSourceType.CAMERA);
        //   }
        // },
        {
          text: 'Cancelar',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public goToTermsAndConditions() {
    this.events.publish('pdf:open', 'PublicFile', { targetUrl: "https://s3.amazonaws.com/snaaplicativo/politica/1548271877_3oEcZgS97k_termos_e_condicoes_gerais_de_uso_e_ou_de_venda+(1).pdf" });
  }

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 10,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      alert('Ocorreu algum erro, tente novamente.');
    });
  }

  // Create a new name for the image
  private createFileName() {
    var d = new Date(),
    n = d.getTime(),
    newFileName =  n + ".jpg";
    return newFileName;
  }

  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(fileEntry => {
      this.intendedPhotoSrc = normalizeURL(fileEntry.nativeURL);
      this.others.push({
        fileKey: "file",
        fileName: newFileName,
        chunkedMode: false,
        mimeType: "multipart/form-data",
        params : { 'file': newFileName }
      });
      this.others.push(fileEntry.nativeURL);
      this.labelFile = "Alterar";
    }, error => {
      alert('Erro ao salvar o arquivo.');
    });
  }

}
