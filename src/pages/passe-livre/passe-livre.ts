import { Component } from '@angular/core';
import { IonicPage, Events, LoadingController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { SessionProvider } from '../../providers/session/session';

@IonicPage()
@Component({
  selector: 'page-passe-livre',
  templateUrl: 'passe-livre.html',
})
export class PasseLivrePage {

  private loading;

  constructor(private api: ApiProvider,
              private events: Events,
              public loadingCtrl: LoadingController,
              public session: SessionProvider) { }

  public goToPasseLivreAvianca(event) {
    this.showLoading();
    this.session.getUser().then(user => {
      if (user && user.cpf && user.anaccode) {
        this.api.getFreePassAviancaLoginUrl(user.cpf, user.anaccode).subscribe(
          urlLogin => {
            this.events.publish('inapp:open', 'Avianca', { targetUrl: urlLogin });
            this.hideLoading();
          }, errorMessage => {
            alert(errorMessage);
            this.hideLoading();
          }
        );
      } else {
        alert("Não foi possível processar sua requisição, tente novamente.");
        this.hideLoading();
      }
    });
  }

  public goToPasseLivreGol(event) {
    this.showLoading();
    this.session.getUser().then(user => {
      if (user && user.cpf && user.anaccode) {
        this.api.getFreePassGolLoginUrl(user.cpf, user.anaccode).subscribe(
          urlLogin => {
            this.events.publish('inapp:open', 'Gol', { targetUrl: urlLogin });
            this.hideLoading();
          }, errorMessage => {
            alert(errorMessage);
            this.hideLoading();
          }
        );
      } else {
        alert("Não foi possível processar sua requisição, tente novamente.");
        this.hideLoading();
      }
    });
  }

  public goToPasseLivreAzul(event) {
    this.showLoading();
    this.session.getUser().then(user => {
      if (user && user.cpf && user.anaccode) {
         this.api.getFreePassAzulLoginUrl(user.cpf.replace(/[^0-9]/g, ''), user.anaccode).subscribe(
          urlLogin => {
            this.events.publish('inapp:open', 'Azul', { targetUrl: urlLogin });
            this.hideLoading();
          }, errorMessage => {
            alert(errorMessage);
            this.hideLoading();
          }
        ); 
      } else {
        this.hideLoading();
        this.events.publish('inapp:open', 'Azul', { targetUrl: 'https://apps.voeazul.com.br/passelivreintegrado/Login.aspx'});
      }
    });
  }

  public goToPasseLivreLatam(event) {
    this.events.publish('inapp:open', 'Latam', { targetUrl: 'http://passelivre.latam.com/PasseLivre-1.0' });
  }

  public goToPasseLivrePassaredo(event) {
    let title = "Passaredo";
    let description = "Os tripulantes, cumprindo com os requisitos gerais da CCT, deverão se apresentar no balcão de check in da companhia, portando uma xerox do crachá que deverá ser entregue ao funcionário que está efetuando o atendimento. A bagagem obrigatoriamente deverá ser despachada. O critério para desempate no embarque utilizado será o de hierarquia funcional (Comandante, Copiloto, Mecânico de Voo e Comissário).";
    this.goToDetails(title, description);
  }

  public goToPasseLivreRio(event) {
    let title = "Rio";
    let description = "Os tripulantes, cumprindo com os requisitos gerais da CCT, deverão se dirigir diretamente ao Comandante da aeronave para verificar a disponibilidade de assentos. O critério adotado será por hierarquia (Comandante, Copiloto, Mecânico de Voo e Comissário). Em qualquer outra situação, o critério será estabelecido pelo Comandante do Voo. Em voos do Banco Central, por força de contrato e segurança, toda e qualquer pessoa deverá obter autorização da Diretoria de Operações para pleitear o seu embarque.";
    this.goToDetails(title, description);
  }

  public goToPasseLivreSideral(event) {
    let title = "Sideral";
    let description = "Os tripulantes, cumprindo com os requisitos gerais da CCT, deverão se dirigir diretamente ao Comandante da aeronave para verificar a disponibilidade de assentos. O critério adotado será por ordem de chegada e como desempate será utilizado será utilizada a hierarquia (Comandante, Copiloto, Mecânico de Voo e Comissário). Em qualquer outra situação, o critério será estabelecido pelo Comandante do Voo. Em voos do Banco Central, por força de contrato e segurança, toda e qualquer pessoa deverá obter autorização da Diretoria de Operações para pleitear o seu embarque.";
    this.goToDetails(title, description);
  }

  public goToPasseLivreTotal(event) {
    let title = "Total Linhas Aéreas";
    let description = "O passe livre deve ser solicitado para o e-mail: operacoes@total.com.br das 8h às 17h de segunda a sexta-feira. No e-mail, deverão constar as informações: Anac, Companhia, trecho, CMA válido e telefone para contato. Devido à limitação de espaço na cabine e na área adjacente à porta dianteira da aeronave, a bagagem do tripulante extra estará limitada a 1 (uma) mala (padrão Tripulante) e mais 1 (uma) bagagem de mão padrão bolsa ou mochila.";
    this.goToDetails(title, description);
  }

  public goToPasseLivreLatamCargo(event) {
    let title = "LATAM Cargo";
    let description = "Os tripulantes, cumprindo os requisitos gerais da CCT, deverão seguir as seguintes regras:<br>– Envio de e-mail com 24 horas de antecedência informando data e se souber o voo pretendido para coordvcp@tamcargo.com.br ;<br>-Será enviado um link para preenchimento dos dados para embarque de acordo com a informação do voo, disponibilidade e prioridade de embarque;<br>-Considerando a possibilidade de mudanças de horário, itinerário ou até mesmo o cancelamento do voo, o interessado deverá confirmar as informações sobre o voo com antecedência de 03h00 antes ao embarque, apenas através do e-mail coordvcp@tamcargo.com.br;<br>-Após confirmação o nome do tripulante será incluído no manifesto de pessoas a bordo e seu embarque será condicionado à apresentação com antecedência mínima de 00:45 min antes do ETD na aeronave.";
    this.goToDetails(title, description);
  }

  public goToPasseLivreMap(event) {
    let title = "MAP Linhas Aéreas";
    let description = "Os tripulantes, cumprindo com os requisitos gerais da CCT, deverão se apresentar no balcão de check in da companhia, portando seu crachá que deverá ser apresentado, juntamente com sua CHT, ao funcionário que está efetuando o atendimento. A bagagem seguirá a mesma regra prevista para o passageiro pagante, assim como o embarque. O embarque ficará condicionado à disponibilidade de lugar e peso e estará condicionado a cinco tripulantes por voo.";
    this.goToDetails(title, description);
  }

  public goToPasseLivreColt(event) {
    let title = "Colt Cargo";
    let description = "Os tripulantes deverão se dirigir diretamente ao comandante da aeronave para verificar a disponibilidade de assentos, de acordo com os critérios já estabelecidos e condições operacionais do equipamento. Os tripulantes também podem encaminhar um email para coordenacao@coltcargo.com.br ou ligar para (11 3199‐1400) e informar NOME , CANAC, CPF, EMPRESA e TRECHO, solicitando autorização.";
    this.goToDetails(title, description);
  }

  public goToPasseLivreModern(event) {
    let title = "Modern Logistics";
    let description = "O tripulante deve enviar um email para passelivre@modern.com.br informando:<br><br>1. Nome<br>2. Data de nascimento<br>3. Empresa em que trabalha<br>5. Telefone para contato em caso de urgência<br>6. Data e trecho solicitado<br><br>A Modern responderá ao email informando a possibilidade de embarque e a sequência de prioridades até três horas antes do voo. Por motivos de segurança, em alguns tipos de operações, haverá restrição ao transporte de tripulantes no sistema passe livre.";
    this.goToDetails(title, description);
  }

  public goToDetails(title, description) {
    if (!this.isInvalidData(title, description)) {
      this.events.publish('page:open', 'NewDetailsPage', {
        description: description,
        title: title
      });
    }
  }

  public showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Efetuando login...'
    });

    this.loading.present();
  }

  public hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  private isInvalidData(title, description) {
    return (title == '') || (description == '');
  }

}
