import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PasseLivrePage } from './passe-livre';

@NgModule({
  declarations: [
    PasseLivrePage,
  ],
  imports: [
    IonicPageModule.forChild(PasseLivrePage),
  ],
})
export class PasseLivrePageModule {}
