import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AgendarConsultaPage } from './agendar-consulta';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    AgendarConsultaPage,
  ],
  imports: [
    IonicPageModule.forChild(AgendarConsultaPage),
    PipesModule
  ],
})
export class AgendarConsultaPageModule {}
