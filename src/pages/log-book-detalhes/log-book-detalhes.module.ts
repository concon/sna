import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LogBookDetalhesPage } from './log-book-detalhes';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    LogBookDetalhesPage,
  ],
  imports: [
    IonicPageModule.forChild(LogBookDetalhesPage),
    PipesModule
  ],
})
export class LogBookDetalhesPageModule {}
