import { Component } from '@angular/core';
import { IonicPage, NavParams, LoadingController, Events } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { LogbookModel } from '../../app/models/logbook-model';

@IonicPage()
@Component({
  selector: 'page-log-book-detalhes',
  templateUrl: 'log-book-detalhes.html',
})
export class LogBookDetalhesPage {

  public logbook: LogbookModel;
  private loading;

  constructor(private navParams: NavParams,
              private api: ApiProvider,
              private loadingCtrl: LoadingController,
              private events: Events) {
    this.logbook = this.navParams.get('logbookData');
  }

  ionViewWillEnter() {
    const updatedData = this.navParams.get('updatedData');
    if (updatedData) {
      this.logbook = updatedData;
    }
  }

  public deletaItem() {
    this.showLoading();
    if (confirm('Tem certeza de que deseja excluir o item do logbook?')) {
      this.api.doLogbookDelete(this.logbook.id).subscribe(
        logbookData => {
          alert("Item do logbook excluido com sucesso");
          this.closePage();
          this.hideLoading();
        }, message => {
          alert(message);
          this.hideLoading();
        }
      );
    }
  }

  public closePage() {
    this.events.publish('page:back');
  }

  public editItem() {
    this.events.publish('page:open', 'LogbookManualPage', {
      logbookData: this.logbook
    });
  }

  public getDate(dateArray) {
    if (dateArray) {
      var year = dateArray[0];
      var month = dateArray[1];
      var day = dateArray[2];

      return month + "/" + day + "/" + year;
    } else {
      return "";
    }
    // MM/dd/yyyy
  }

  public showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });

   this.loading.present();
  }

  public hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

}
