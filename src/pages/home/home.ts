import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { IonicPage, Platform, Events, Searchbar } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { ArticleModel } from '../../app/models/article-model';
import { SessionProvider } from '../../providers/session/session';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements AfterViewInit {

  @ViewChild('searchbar') searchbar:Searchbar;

  public articleList: ArticleModel[];
  public searchInput;
  public isSearchBarOpenned: boolean = false;
  public buttonClicked: boolean = false;
  public userModel;

  constructor(public platform: Platform,
              public api: ApiProvider,
              public session: SessionProvider,
              private events: Events) {
    this.initScreenPlaceHolder();
    this.configureToolbarOptions();
  }

  ngAfterViewInit() {
    this.platform.ready().then(() => {
       this.loadLatestNews(null);
    });
  }

  private configureToolbarOptions() {
    this.session.sessionDataObservable.subscribe(sessionModel => {
      if (sessionModel) {
        this.userModel = sessionModel.user;
      } else {
        this.userModel = null;
      }
    });
  }

  private initScreenPlaceHolder() {
    this.articleList = [new ArticleModel(), new ArticleModel(), new ArticleModel()];
  }

  public onSearchClick() {
    if (this.searchInput) {
      this.events.publish('page:open', 'NewsPage', { searchInput: this.searchInput });
      this.onButtonClick();
      this.searchInput = "";
    }
  }

  public onProfileClick() {
    this.events.publish('page:open', 'PerfilPage', null);
  }

  public onLoginClick() {
    this.events.publish('page:open', 'LoginPage', null);
  }

  public onButtonClick() {
    if (!this.isSearchBarOpenned) {
      setTimeout(() => {
        this.searchbar.setFocus();
      });
    }

    this.isSearchBarOpenned = !this.isSearchBarOpenned;
  }

  public goToNewsPage(event) {
    this.events.publish('page:open', 'NewsPage', null);
  }
  public goToServicesPage(event) {
    this.events.publish('page:open', 'ServicesPage', null);
  }
  public goToParceriasPage(event) {
    this.events.publish('page:open', 'ParceriasPage', null);
  }
  public goToCarreirasPage(event) {
    this.events.publish('page:open', 'CarreirasPage', null);
  }
  public goToAssembleiasPage(event) {
    this.events.publish('page:open', 'AssembleiasPage', null);
  }
  public goToPasseLivrePage(event) {
    this.events.publish('page:open', 'PasseLivrePage', null);
  }
  public goToCarteirinhaExternalPage(event) {
    this.events.publish('page:open', 'CarteirinhaVirtualPage', null);
  }
  public goToCalculoJornadaPage(event) {
    this.events.publish('page:open', 'CalculoJornadaPage', null);
  }
  public goToSolicitarAtendimentoPage(event, tipoAtendimento) {
    this.events.publish('page:open', 'ListaSolicitacoesPage', { tipoAtendimento: tipoAtendimento });
  }
  public goToListaCalculoSalarioPage(event) {
    this.events.publish('page:open', 'ListaSalarioPage', null);
  }
  public goToLogBookListaPage(event) {
    this.events.publish('page:open', 'LogBookListaPage', null);
  }

  public loadLatestNews(refresher) {
    this.api.getLatestNews().subscribe(
      articles => {
        this.articleList = articles;
        if (refresher) refresher.complete();
      }, errorMessage => {
        if (refresher) refresher.complete();
      }
    );
  }

  public doRefresh(refresher) {
    this.initScreenPlaceHolder();
    this.loadLatestNews(refresher);
  }

}
