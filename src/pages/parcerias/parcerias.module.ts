import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ParceriasPage } from './parcerias';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    ParceriasPage,
  ],
  imports: [
    IonicPageModule.forChild(ParceriasPage),
    ComponentsModule
  ]
})
export class ParceriasPageModule {}
