import { Component } from '@angular/core';
import { Platform, IonicPage } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { ArticleModel } from '../../app/models/article-model';
import { CategoryModel } from '../../app/models/category-model';

@IonicPage()
@Component({
  selector: 'page-parcerias',
  templateUrl: 'parcerias.html',
})
export class ParceriasPage {

  private offset = 0;
  private offsetValue = 5;
  public isLoadingNews = false;
  public articleList: ArticleModel[];
  public stateList: CategoryModel[];
  public typeList: CategoryModel[];
  public categoryId = "";
  public typeId = "";
  public showFilter = true;
  public dataHasBeenLoaded = false;

  constructor(public platform: Platform,
              public api: ApiProvider) { }

  ngAfterViewInit() {
    this.platform.ready().then(() => {
      this.loadStates();
    });
  }

  public onStateSelectChange(event) {
    this.clearScreenPlaceHolder();
    this.offset = 0;
    this.offsetValue = 5;
    this.loadType();
  }

  public onTypeSelectChange(event) {
    this.initScreenPlaceHolder();
    this.offset = 0;
    this.offsetValue = 5;
    this.loadLatestNews(null, 10);
  }

  private initScreenPlaceHolder() {
    this.articleList = [new ArticleModel(), new ArticleModel(), new ArticleModel()];
  }

  private clearScreenPlaceHolder() {
    this.articleList = [];
    this.dataHasBeenLoaded = false;
  }

  public loadStates() {
    this.api.getStateList().subscribe(
      states => {
        this.stateList = states;
      }, errorMessage => {
        alert(errorMessage);
      }
    );
  }

  public loadType() {
    this.api.getTypeList(this.categoryId).subscribe(
      types => {
        this.typeList = types;
      }, errorMessage => {
        alert(errorMessage);
      }
    );
  }

  public loadLatestNews(infiniteScroll, quantity) {
    this.dataHasBeenLoaded = false;
    this.api.getPartnershipsWithOffset(this.offset, quantity, this.typeId).subscribe(
      articles => {
        if (this.offset == 0) {
          this.articleList = articles;
          this.offset = quantity;
        } else {
          this.linkList(articles);
        }
        if (infiniteScroll) infiniteScroll.complete();
        this.dataHasBeenLoaded = true;
        this.isLoadingNews = false;
      }, errorMessage => {
        if (infiniteScroll) infiniteScroll.complete();
        this.dataHasBeenLoaded = true;
        this.isLoadingNews = false;
      }
    );
  }

  private linkList(receivedArticleList) {
    this.articleList = this.articleList.concat(receivedArticleList);
  }

  public doInfinite(infiniteScroll) {
    if (!this.isLoadingNews && this.stateList) {
      this.isLoadingNews = true;

      this.offset += this.offsetValue;
      this.loadLatestNews(infiniteScroll, 5);
    }
  }

  public openChangeImageModal() {
    this.showFilter = !this.showFilter;
  }

}
