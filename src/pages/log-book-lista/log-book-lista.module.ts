import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LogBookListaPage } from './log-book-lista';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    LogBookListaPage,
  ],
  imports: [
    IonicPageModule.forChild(LogBookListaPage),
    ComponentsModule,
    PipesModule
  ],
})
export class LogBookListaPageModule {}
