import { Component, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { Platform, IonicPage, Events, LoadingController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { LogbookModel } from '../../app/models/logbook-model';

declare var google;

@IonicPage()
@Component({
  selector: 'page-log-book-lista',
  templateUrl: 'log-book-lista.html',
})
export class LogBookListaPage implements AfterViewInit {

  private page = 0;
  private size = 5;
  public isLoadingData = false;

  @ViewChild('currencychart') currencyChart: ElementRef;
  public logbookList;
  public logbookSummary;
  private loading;
  public dataHasBeenLoaded = false;
  public logbookTabs: String = 'resumo';

  constructor(public platform: Platform,
              public api: ApiProvider,
              public loadingCtrl: LoadingController,
              private events: Events) {
    this.initScreenPlaceHolder();

  }

  ngAfterViewInit() {

  }

  ionViewWillEnter() {
    this.platform.ready().then(() => {
      this.changeSegment();
    });
  }

  public swipeEvent(e, tab) {
    if (e.direction == 2 && tab === 'resumo') {
      this.logbookTabs = 'lista';
    }
    if (e.direction == 4 && tab === 'lista') {
      this.logbookTabs = 'resumo';
    }
  }

  public changeSegment() {
    if (this.logbookTabs == 'resumo') {
      this.loadLogbookSummary();
    } else {
      this.page = 0;
      this.initScreenPlaceHolder();
      this.loadLogbookList(null, this.size);
    }
  }

  public initScreenPlaceHolder() {
    this.logbookList = [new LogbookModel(), new LogbookModel(), new LogbookModel()];
  }

  public loadLogbookList(infiniteScroll, size) {
    this.dataHasBeenLoaded = false;

    this.api.getLogbooks(this.page, size).subscribe(
      loogbooks => {
        if (this.page == 0) {
          this.logbookList = loogbooks;
        } else {
          this.linkList(loogbooks);
        }

        this.dataHasBeenLoaded = true;

        if (infiniteScroll) infiniteScroll.complete();
        this.isLoadingData = false;
      }, errorMessage => {
        alert(errorMessage);
        this.logbookList = [];

        this.dataHasBeenLoaded = true;

        if (infiniteScroll) infiniteScroll.complete();
        this.isLoadingData = false;
      }
    );
  }

  private linkList(receivedArticleList) {
    this.logbookList = this.logbookList.concat(receivedArticleList);
  }

  public doInfinite(infiniteScroll) {
    if (!this.isLoadingData) {
      this.isLoadingData = true;

      this.page++;
      this.loadLogbookList(infiniteScroll, 5);
    }
  }

  public loadLogbookSummary() {
    this.dataHasBeenLoaded = false;

    this.showLoading();
    this.api.getLogbookSummary().subscribe(
      logbookSummaryData => {
        this.logbookSummary = logbookSummaryData;
        this.loadCurrencyChart(this.logbookSummary.currency.day, this.logbookSummary.currency.night, this.logbookSummary.currency.instrument);

        this.dataHasBeenLoaded = true;
        this.hideLoading();
      }, errorMessage => {
        alert(errorMessage);

        this.dataHasBeenLoaded = true;
        this.hideLoading();
      }
    );
  }

  public loadCurrencyChart(day, night, instrument) {
    setTimeout(() => {
      let data = google.visualization.arrayToDataTable([
        ['Tipo', ''],
        ['Dia', day],
        ['Noite', night],
        ['Instrument', instrument],
      ]);

      let options = {
        title: 'Currency',
        chartArea: { left: 0, right: 0, top: 0, width: '100%', height: '75%' },
        pieHole: 1,
        fontSize: 16,
        animation: { duration: 1000, easing: 'out', startup: true },
        legend: { position: 'bottom', textStyle: { color: '#666', fontSize: 14 } },
        slices: { 0: { color: '#4584f3' }, 1: { color: '#0165b1' } }
      };

      if (this.currencyChart && this.currencyChart.nativeElement) {
        var chart = new google.visualization.PieChart(this.currencyChart.nativeElement);
        chart.draw(data, options);
      }
    }, 100);
  }

  public showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });

    this.loading.present();
  }

  public hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  public goToLogbookReport() {
    this.events.publish('page:open', 'LogbookReportPage', null);
  }


  public goToLogBookManualPage(event) {
    this.events.publish('page:open', 'LogbookManualPage', null);
  }

}
