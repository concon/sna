import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AcoesColetivasPage } from './acoes-coletivas';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    AcoesColetivasPage,
  ],
  imports: [
    IonicPageModule.forChild(AcoesColetivasPage),
    ComponentsModule
  ],
})
export class AcoesColetivasPageModule {}
