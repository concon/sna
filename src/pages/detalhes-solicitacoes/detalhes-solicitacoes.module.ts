import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetalhesSolicitacoesPage } from './detalhes-solicitacoes';

@NgModule({
  declarations: [
    DetalhesSolicitacoesPage,
  ],
  imports: [
    IonicPageModule.forChild(DetalhesSolicitacoesPage),
  ],
})
export class DetalhesSolicitacoesPageModule {}
