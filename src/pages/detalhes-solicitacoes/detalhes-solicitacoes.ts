import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, AlertController, NavParams, Platform, LoadingController, Events } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { SessionProvider } from '../../providers/session/session';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/first';

import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';

declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-detalhes-solicitacoes',
  templateUrl: 'detalhes-solicitacoes.html',
})
export class DetalhesSolicitacoesPage {

  @ViewChild('fileInp') fileInput: ElementRef;
  private selectedFiles = [];
  private isNativePlatform = false;

  public loading;
  public id;
  public data;
  public userModel;

  constructor(
    public navParams: NavParams,
    public api: ApiProvider,
    public filePath: FilePath,
    private events: Events,
    public file: File,
    public alertController: AlertController,
    public loadingCtrl: LoadingController,
    public session: SessionProvider,
    private platform: Platform) {

    this.id = this.navParams.get('id');

  }

  ionViewWillEnter() {
    this.platform.ready().then(() => {
      this.session.sessionDataObservable.subscribe(sessionModel => {
        if (sessionModel) {
          this.userModel = sessionModel.user;
        } else {
          this.userModel = null;
        }
      });
      this.loadDetailsById();
    });
  }

  ngAfterViewInit() {
    this.platform.ready().then(() => {
        this.isNativePlatform = !(this.platform.is('mobileweb') || this.platform.is('android'));
    });
  }

  private loadDetailsById() {
    this.showLoading();
    this.api.getRequestDetails(this.id).subscribe(
      requestModel => {
        this.data = requestModel;
        this.hideLoading();
      }, errorMessage => {
        alert(errorMessage);
        this.hideLoading();
      }
    );
  }

  public getDate(date) {
    return (date) ? date[2] + '/' + date[1] + '/' + date[0] : '';
  }

  public doAnswerRequest(request) {
    this.showLoading();
  	this.api.doAnswerRequest(request).subscribe(
      res => {
    		alert("Mensagem enviada com sucesso!");
    		this.hideLoading();
        this.loadDetailsById();
      }, message => {
        alert(message);
        this.hideLoading();
      }
    );
  }

  public doCloseRequest() {
    this.showLoading();
  	this.api.doCloseRequest(this.id).subscribe(
      res => {
    		alert("Solicitação encerrada com sucesso!");
    		this.hideLoading();
        this.loadDetailsById();
      }, message => {
        alert(message);
        this.hideLoading();
      }
    );
  }

  public doRemoveFile(id) {
    this.showLoading();
  	this.api.doRemoveFileFromRequest(id).subscribe(
      res => {
    		alert("Arquivo removido com sucesso!");
    		this.hideLoading();
        this.loadDetailsById();
      }, message => {
        alert(message);
        this.hideLoading();
      }
    );
  }

  public sendMessage() {
    let alertCtrl = this.alertController.create({
      title: 'Responder',
      inputs: [
        {
          name: 'message',
          placeholder: 'insira o texto aqui',
          type: 'text'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
          }
        },
        {
          text: 'Enviar',
          handler: data => {
            if (data['message'] && data['message'].length > 0) {
              var request = {
                "associado": true,
                "atendimento": this.id[0],
                "texto": data['message'],
                "time": ''
              };
              this.doAnswerRequest(request);
            } else {
              alert('Preencha o campo corretamente.');
            }
          }
        }
      ]
    });
    alertCtrl.present();
  }

  // iOS only, remove a created copy of file from temporary folder
  public removeLocalFile(others) {
    if (this.isNativePlatform && others && others[0] && others[0].fileName) {
      this.file.removeFile(cordova.file.dataDirectory, others[0].fileName).catch(err => console.log(err));
    }
  }

  // Orchestrate files uploading, using a FIFO concept
  // Stock all file errors and show at the end of process
  private async orchestrateFilesUploading(requestId) {
    let errorFiles = [];

    this.showLoading();
    for(let selectedFile of this.selectedFiles) {
      var preparedFile = await this.prepareUploadingFile(selectedFile);

      try {
        await this.doUploadFile(requestId, preparedFile[0], preparedFile[1], selectedFile.name);
      } catch(errorMessage) {
        errorFiles.push(selectedFile.name);
      }
      this.removeLocalFile(preparedFile[1]);
    }

    if (errorFiles.length > 0) {
      alert("Ocorreu algum erro no upload do(s) arquivo(s): " + errorFiles.join(", ") + ". Tente adicioná-lo(s) mais tarde.");
    } else {
      alert("Novo arquivo adicionado com sucesso!");
    }
    this.hideLoading();
    this.loadDetailsById();
  }

  // Return a promise to synchrounous file upload
  async doUploadFile(requestId, fileFormData, others, nome): Promise<any> {
   return new Observable((observer) => {
    	this.api.doNewRequestUploadFile(requestId, fileFormData, others, nome).subscribe(
        res => {
        	observer.next();
        }, message => {
          observer.error();
        }
      );
    }).first().toPromise();
  }

  // Remove file from array
  public removeFile(id) {
    this.doRemoveFile(id);
  }

  // Add new file to array
  public fileChange(event) {
    let files = event.target.files;

    for(let file of files) {
      this.selectedFiles.push(file);
    }

    this.orchestrateFilesUploading(this.data.id);
  }

  // Call native file manager
  public addFileNextStep() {
    this.fileInput.nativeElement.click();
  }

  // Prepare file to upload based on working rule of Android (webFormData) and iOS (NativePlatform)
  private async prepareUploadingFile(file) {
    var webFormData:FormData = new FormData();
    var nativeFormData = [];

    if (this.isNativePlatform) {
      await this.file.writeFile(cordova.file.dataDirectory, file.name, file, { replace: true }).then((fileEntry) => {
        nativeFormData.push({
          fileKey: "file",
          fileName: file.name,
          chunkedMode: false,
          mimeType: "multipart/form-data",
          params : {'file': file.name}
        });
        nativeFormData.push(cordova.file.dataDirectory + file.name);
      }).catch((err) => {
        alert("Não foi possível anexar o arquivo.");
      });
    } else {
      webFormData.append('file', file);
    }

    return [webFormData, nativeFormData];
  }

  public openPDF(url) {
    this.events.publish('pdf:open', 'PublicFile', { targetUrl: url });
  }

  public showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });

    this.loading.present();
  }

  public hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }


}
