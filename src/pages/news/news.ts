import { Component, ViewChild } from '@angular/core';
import { Platform, IonicPage, NavParams, Searchbar, LoadingController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { ArticleModel } from '../../app/models/article-model';

@IonicPage()
@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
})
export class NewsPage {

  @ViewChild('searchbar') searchbar:Searchbar;

  private loading;
  public searchInput;
  public previousSearchInput;
  public isSearchBarOpenned: boolean = false;

  private offset = 0;
  private offsetValue = 5;
  private isLoadingNews = false;

  public articleList: ArticleModel[];
  public searchedArticleList: ArticleModel[];

  constructor(public platform: Platform,
              public navParams: NavParams,
              public api: ApiProvider,
              public loadingCtrl: LoadingController) {
    this.searchInput = this.navParams.get('searchInput');

    this.initScreenPlaceHolder();
  }

  ngAfterViewInit() {
    this.platform.ready().then(() => {
      if (!this.searchInput) {
        this.loadLatestNews(null, 10);
      } else {
        this.onButtonClick();
        this.searchNews();
      }
    });
  }

  ionViewWillLeave() {
    //this.hideLoading();
  }

  public loadNewsControl() {
    this.initScreenPlaceHolder();
    if (this.searchInput) {
      this.searchNews();
    } else {
      this.loadLatestNews(null, 10);
    }
  }

  private initScreenPlaceHolder() {
    if (!this.searchInput) {
      this.searchedArticleList = [];
      this.articleList = [new ArticleModel(), new ArticleModel(), new ArticleModel()];
    } else {
      this.articleList = [];
      this.searchedArticleList = [new ArticleModel(), new ArticleModel(), new ArticleModel()];
    }
    this.offset = 0;
  }

  public searchNews() {
    this.showLoading();
    this.api.doSearchNews(this.searchInput).subscribe(
      articles => {
        this.searchedArticleList = articles;
        this.hideLoading();
      }, errorMessage => {
        alert(errorMessage);
        this.searchedArticleList = [];
        this.hideLoading();
      }
    );
  }

  public loadLatestNews(infiniteScroll, quantity) {
    this.api.getNewsWithOffset(this.offset, quantity).subscribe(
      articles => {
        if (this.offset == 0) {
          this.articleList = articles;
          this.offset = quantity;
        } else {
          this.linkList(articles);
        }

        if (infiniteScroll) infiniteScroll.complete();
        this.isLoadingNews = false;
      }, errorMessage => {
        if (infiniteScroll) infiniteScroll.complete();
        this.isLoadingNews = false;
      }
    );
  }

  private linkList(receivedArticleList) {
    this.articleList = this.articleList.concat(receivedArticleList);
  }

  public doInfinite(infiniteScroll) {
    if (!this.isLoadingNews) {
      this.isLoadingNews = true;

      this.offset += this.offsetValue;
      this.loadLatestNews(infiniteScroll, 5);
    }
  }

  public onButtonClick() {
    if (!this.isSearchBarOpenned) {
      setTimeout(() => {
        this.searchbar.setFocus();
      });
    }

    this.isSearchBarOpenned = !this.isSearchBarOpenned;
  }

  public showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });

    this.loading.present();
  }

  public hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

}
