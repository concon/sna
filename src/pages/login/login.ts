import { Component } from '@angular/core';
import { IonicPage, Platform, NavParams, ToastController, Events, AlertController, LoadingController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { SessionProvider } from '../../providers/session/session';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  private targetType;
  private targetPage;
  private targetParams;
  private loading;
  public loginForm: FormGroup;
  public hasFormError = false;
  public registrationSessionData;
  public storePersonalDataAgreement = false;

  constructor(public platform: Platform,
              public alertCtrl: AlertController,
              public toastCtrl: ToastController,
              public navParams: NavParams,
              public api: ApiProvider,
              public events: Events,
              public session: SessionProvider,
              public loadingCtrl: LoadingController) {
    this.targetType = this.navParams.get('targetType');
    this.targetPage = this.navParams.get('targetPage');
    this.targetParams = this.navParams.get('targetParams');

    this.loginForm = new FormGroup({
      cpf: new FormControl('', [Validators.required, Validators.minLength(13)]),
      senha: new FormControl('', [Validators.required, Validators.minLength(1)])
    });
  }

  public doLogin() {
    this.hasFormError = !this.loginForm.valid;
    if (this.loginForm.valid) {
      this.showLoading();
    	this.api.doLogin(this.loginForm.value).subscribe(
        res => {
          this.createUserDataSession(res).then(res => {
            this.hideLoading();
            this.flowToTarget();
          });
        }, message => {
          alert(message);
          this.hideLoading();
        }
      );
    }
  }

  ionViewWillEnter() {
    if (this.registrationSessionData) {
      this.showLoading();
      this.createUserDataSession(this.registrationSessionData).then(res => {
        this.hideLoading();
        this.flowToTarget();
      });
    }
  }

  public registrationCallback = (sessionData) => {
    return new Promise((resolve, reject) => {
      this.registrationSessionData = sessionData;
      resolve();
    });
  }

  private flowToTarget() {
    if (this.targetType == "page") {
      if (this.targetPage != "LoginPage") {
        this.events.publish('page:open', this.targetPage, this.targetParams);
      }
    } else if (this.targetType == "inapp") {
      this.events.publish('inapp:open', this.targetPage, this.targetParams);
    } else if (this.targetType == "pdf") {
      this.events.publish('pdf:open', this.targetPage, this.targetParams);
    }

    this.closeLoginModal();
  }

  public goToTermsAndConditions() {
    this.events.publish('pdf:open', 'PublicFile', { targetUrl: "https://s3.amazonaws.com/snaaplicativo/politica/1548271877_3oEcZgS97k_termos_e_condicoes_gerais_de_uso_e_ou_de_venda+(1).pdf" });
  }

  public createUserDataSession(sessionModel) {
    return this.session.create(sessionModel);
  }

  public closeLoginModal() {
    this.events.publish('page:closePage', "LoginPage");
  }

  public showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Efetuando login...'
    });

    this.loading.present();
  }

  public goToCadastro() {
    this.events.publish('page:open', 'CadastroPage', { loginCallback: this.registrationCallback });
  }

  public hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  public forgotPass() {
    let forgot = this.alertCtrl.create({
      title: 'Esqueceu sua senha?',
      message: "Coloque seu email aqui para recuperar.",
      inputs: [
        {
          name: 'email',
          placeholder: 'Email',
          type: 'email'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          handler: data => {
          }
        },
        {
          text: 'Enviar',
          handler: data => {
            let toast = this.toastCtrl.create({
              message: 'Email enviado com sucesso!',
              duration: 3000,
              position: 'top',
              cssClass: 'dark-trans',
              closeButtonText: 'OK',
              showCloseButton: true
            });
            toast.present();
          }
        }
      ]
    });
    forgot.present();
  }

}
