import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListaSolicitacoesPage } from './lista-solicitacoes';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    ListaSolicitacoesPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaSolicitacoesPage),
    ComponentsModule
  ],
})
export class ListaSolicitacoesPageModule {}
