import { Component } from '@angular/core';
import { Platform, IonicPage, Events, LoadingController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { RequestModel } from '../../app/models/request-model';

@IonicPage()
@Component({
  selector: 'page-lista-solicitacoes',
  templateUrl: 'lista-solicitacoes.html',
})
export class ListaSolicitacoesPage {

  private loading;
  public dataHasBeenLoaded = false;
  public requestList: RequestModel[];
  public tipo;
  public requestType;

  constructor(private platform: Platform,
              private api: ApiProvider,
              private events: Events,
              public loadingCtrl: LoadingController,
              public navParams: NavParams) {

    this.initScreenPlaceHolder();

    this.requestType = this.navParams.get('tipoAtendimento');

    if (this.requestType == "DENUNCIA") {
      this.tipo = "Denúncia";
    } else if (this.requestType == "RELPREV") {
      this.tipo = "Relprev";
    } else if (this.requestType == "JURIDICO") {
      this.tipo = "Jurídico";
    }
  }

  public goToSolicitarAtendimentoPage(event) {
    this.events.publish('page:open', 'SolicitarAtendimentoPage', { tipoAtendimento: this.requestType });
  }

  ngAfterViewInit() { }

  ionViewWillEnter() {
    this.platform.ready().then(() => {
       this.loadRequestList();
    });
  }

  private initScreenPlaceHolder() {
    this.requestList = [new RequestModel(), new RequestModel(), new RequestModel()];
  }

  public loadRequestList() {
    this.dataHasBeenLoaded = false;
    this.showLoading();
    this.api.getRequests(this.requestType).subscribe(
      requests => {
        this.requestList = requests;

        this.dataHasBeenLoaded = true;
        this.hideLoading();
      }, errorMessage => {
        alert(errorMessage);
        this.dataHasBeenLoaded = true;
        this.hideLoading();
      }
    );
  }

  public showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });

    this.loading.present();
  }

  public hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

}
