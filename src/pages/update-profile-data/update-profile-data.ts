import { Component } from '@angular/core';
import { IonicPage, Platform, NavController, NavParams, ToastController, Events, AlertController, LoadingController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { SessionProvider } from '../../providers/session/session';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-update-profile-data',
  templateUrl: 'update-profile-data.html',
})
export class UpdateProfileDataPage {

	private targetType;
  private targetPage;
  private targetParams;
  private loading;
  public profileForm: FormGroup;
  public hasFormError = false;

  public airlineList;

  public cargosList;

  constructor(public platform: Platform,
              public navCtrl: NavController,
              public forgotCtrl: AlertController,
              public toastCtrl: ToastController,
              public navParams: NavParams,
              public api: ApiProvider,
              public events: Events,
              public session: SessionProvider,
              public loadingCtrl: LoadingController) {
  	this.targetType = this.navParams.get('targetType');
    this.targetPage = this.navParams.get('targetPage');
    this.targetParams = this.navParams.get('targetParams');

    this.profileForm = new FormGroup({
      anaccode: new FormControl('', [Validators.required, Validators.minLength(3)]),
      companhia: new FormControl('', [Validators.required]),
      cargo: new FormControl('', [Validators.required])
    });
  }

  ngAfterViewInit() {
    this.platform.ready().then(() => {
      this.loadAirlineList();
      this.loadCargoList();
    });
  }

  private loadAirlineList() {
    this.showLoading('Carregando dados...');
    this.api.getAirlineList().subscribe(
      airlines => {
        this.airlineList = airlines;
        this.fillUpDataFields();
        this.hideLoading();
      }, errorMessage => {
        alert(errorMessage);
        this.hideLoading();
      }
    );
  }

  private loadCargoList() {
    this.api.getCargos().subscribe(
      carg => {
        this.cargosList = carg;
        this.fillUpDataFields();
      }, errorMessage => {
        alert(errorMessage);
      }

    );
  }



  public fillUpDataFields() {
    this.session.getUser().then(user => {
      if (user) {
        let anaccode = (user.anaccode) ? user.anaccode : '';
        let companhia = (user.companhia) ? user.companhia : '';
        let cargo = (user.cargo) ? user.cargo : '';
        this.profileForm.controls['anaccode'].setValue(anaccode);
        this.profileForm.controls['companhia'].setValue(companhia.id);
        this.profileForm.controls['cargo'].setValue(cargo.id);
      }
    });
  }

  public doUpdate() {
    this.hasFormError = !this.profileForm.valid;
    if (this.profileForm.valid) {
      this.showLoading('Salvando dados...');
    	this.api.doUserUpdate(this.profileForm.value).subscribe(
        user => {
          this.updateUserDataSession(user).then(session => {
          	alert("Dados atualizados com sucesso!");
            this.flowToTarget();
          });
          this.hideLoading();
        }, message => {
          alert(message);
          this.hideLoading();
        }
      );
    }
  }

  private flowToTarget() {
    if (this.targetType == "page") {
      if (this.targetPage != "UpdateProfileDataPage") {
        this.events.publish('page:open', this.targetPage, this.targetParams);
      }
    } else if (this.targetType == "inapp") {
      this.events.publish('inapp:open', this.targetPage, this.targetParams);
    } else if (this.targetType == "pdf") {
      this.events.publish('pdf:open', this.targetPage, this.targetParams);
    }

    // close login page
    this.navCtrl.remove(this.navCtrl.getActive().index);
  }

  public updateUserDataSession(userModel) {
    return this.session.updateUserData(userModel);
  }

  public closeModal() {
    this.events.publish('page:back');
  }

  public showLoading(message) {
    this.loading = this.loadingCtrl.create({
      content: message
    });

    this.loading.present();
  }

  public hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

}
