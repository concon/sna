import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { Platform, IonicPage, LoadingController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { SessionProvider } from '../../providers/session/session';
import { VirtualCardModel } from '../../app/models/virtual-card-model';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

@IonicPage()
@Component({
  selector: 'page-carteirinha-virtual',
  templateUrl: 'carteirinha-virtual.html',
})
export class CarteirinhaVirtualPage implements AfterViewInit, OnDestroy {

	private loading;
	public virtualCard: VirtualCardModel;
	public dataHasBeenLoaded = false;

  constructor(public platform: Platform,
              public api: ApiProvider,
              public loadingCtrl: LoadingController,
              public session: SessionProvider,
              private screenOrientation: ScreenOrientation) { }

  ngAfterViewInit() {
    this.platform.ready().then(() => {
      if ((this.platform.is('android') || this.platform.is('ios') || this.platform.is('core')) && !this.platform.is('mobileweb')) {
        this.screenOrientation.unlock();
      }

      this.session.getUser().then(user => {
      	if (user && user.cpf) {
      		this.loadVirtualCardData(user.cpf.replace(/[^0-9]/g, ''));
      	}
      });
    });
  }

  ngOnDestroy() {
    if ((this.platform.is('android') || this.platform.is('ios') || this.platform.is('core')) && !this.platform.is('mobileweb')) {
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    }
  }

  public loadVirtualCardData(cpf) {
  	this.showLoading();
  	this.api.getVirtualCardData(cpf).subscribe(
      virtualCardData => {
        this.virtualCard = virtualCardData;
        this.dataHasBeenLoaded = true;
        this.hideLoading();
      }, errorMessage => {
        alert(errorMessage);
        this.dataHasBeenLoaded = true;
        this.hideLoading();
      }
    );
  }

  public showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });

    this.loading.present();
  }

  public hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

}
