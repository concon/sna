import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CarteirinhaVirtualPage } from './carteirinha-virtual';

@NgModule({
  declarations: [
    CarteirinhaVirtualPage,
  ],
  imports: [
    IonicPageModule.forChild(CarteirinhaVirtualPage),
  ],
})
export class CarteirinhaVirtualPageModule {}
