import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListaSalarioPage } from './lista-salario';
import { ComponentsModule } from '../../components/components.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    ListaSalarioPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaSalarioPage),
    ComponentsModule,
    PipesModule
  ],
})
export class ListaSalarioPageModule {}
