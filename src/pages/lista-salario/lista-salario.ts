import { Component } from '@angular/core';
import { Platform, IonicPage, NavParams, Events } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { WageCalculationUnitModel } from '../../app/models/wage-calculation-unit-model';

@IonicPage()
@Component({
  selector: 'page-lista-salario',
  templateUrl: 'lista-salario.html',
})
export class ListaSalarioPage {

  private page = 0;
  private size = 5;
  public isLoadingData = false;

  public dataHasBeenLoaded = false;
  public wageCalculationList: WageCalculationUnitModel[];

  constructor(public platform: Platform,
              public navParams: NavParams,
              public api: ApiProvider,
              private events: Events) {
    this.initScreenPlaceHolder();
  }

  ngAfterViewInit() {

  }

  ionViewWillEnter() {
    this.platform.ready().then(() => {
      this.page = 0;
      this.initScreenPlaceHolder();
      this.loadWageCalculationList(null, this.size);
    });
  }

  private initScreenPlaceHolder() {
    this.wageCalculationList = [new WageCalculationUnitModel(), new WageCalculationUnitModel(), new WageCalculationUnitModel()];
  }

  public loadWageCalculationList(infiniteScroll, size) {
    this.dataHasBeenLoaded = false;

    this.api.getWageCalculationList(this.page, size).subscribe(
      wageCalculationListData => {
        if (this.page == 0) {
          this.wageCalculationList = wageCalculationListData;
        } else {
          this.linkList(wageCalculationListData);
        }

        this.dataHasBeenLoaded = true;

        if (infiniteScroll) infiniteScroll.complete();
        this.isLoadingData = false;
      }, errorMessage => {
        alert(errorMessage);
        this.wageCalculationList = [];
        this.dataHasBeenLoaded = true;

        if (infiniteScroll) infiniteScroll.complete();
        this.isLoadingData = false;
      }
    );
  }

  private linkList(receivedArticleList) {
    this.wageCalculationList = this.wageCalculationList.concat(receivedArticleList);
  }

  public doInfinite(infiniteScroll) {
    if (!this.isLoadingData) {
      this.isLoadingData = true;

      this.page++;
      this.loadWageCalculationList(infiniteScroll, this.size);
    }
  }

  public goToNewWageCalculation(event) {
    this.events.publish('page:open', 'NovoCalculoSalarioPage', null);
  }

}
