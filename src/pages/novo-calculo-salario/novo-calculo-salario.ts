import { Component, ViewChild, ElementRef } from '@angular/core';
import { Platform, IonicPage, NavParams, LoadingController, Events } from 'ionic-angular';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiProvider } from '../../providers/api/api';

import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';

declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-novo-calculo-salario',
  templateUrl: 'novo-calculo-salario.html',
})
export class NovoCalculoSalarioPage {

  @ViewChild('fileInp') fileInput: ElementRef;
	private loading;
  public requestForm: FormGroup;
  public hasFormError = false;
  public fileName : string;
  public labelFile = "Selecionar arquivo...";
  public fileFormData:FormData = new FormData();
  private others = [];
  public rangeType;

  private isNativePlatform = false;

  constructor(public platform: Platform,
              public navParams: NavParams,
              public api: ApiProvider,
              public events: Events,
              public loadingCtrl: LoadingController,
              public filePath: FilePath,
              public file: File) {
  	this.requestForm = new FormGroup({
      rangeType: new FormControl('', [Validators.required]),
      logbookIncluded: new FormControl(false, [])
    });

    this.closeCalculoSalario();
  }

  ngAfterViewInit() {
    this.platform.ready().then(() => {
        this.isNativePlatform = !(this.platform.is('mobileweb') || this.platform.is('android'));
    });
  }

  public doRequest() {
    this.hasFormError = !(this.requestForm.valid && ((this.others && this.others[0]) || (this.fileFormData && this.fileFormData.has("file") && this.fileFormData.get("file"))));
    if (!this.hasFormError) {
      this.doUploadFile();
    }
  }

  public removeLocalFile() {
    if (this.isNativePlatform && this.others && this.others[0] && this.others[0].fileName) {
      this.file.removeFile(cordova.file.dataDirectory, this.others[0].fileName).catch(err => console.log(err));
    }
  }

  private doUploadFile() {
    this.showLoading();
    this.api.doNewWageCalculationUploadFile(this.requestForm.controls['rangeType'].value, this.requestForm.controls['logbookIncluded'].value, this.fileFormData, this.others).subscribe(
      wageCalculation => {
        alert("Solicitação registrada com sucesso!");

        if (wageCalculation.type == "Planejada") {
          this.events.publish('page:open', 'CalculoSalarioPage', { plannedWageCalculation: wageCalculation });
        } else {
          this.events.publish('page:open', 'CalculoSalarioPage', { executedWageCalculation: wageCalculation });
        }
        this.removeLocalFile();
        this.hideLoading();
      }, message => {
        alert(message);
        this.hideLoading();
      }
    );
  }

  public addFileNextStep() {
    this.addFile();
  }

  public resetFile() {
    this.fileFormData = new FormData();
    this.others = [];
  }

  public fileChange(event) {
    this.resetFile();

    let fileList: FileList = event.target.files;

    if (fileList.length > 0) {
        this.labelFile = "Alterar";
        this.fileName = fileList[0].name;

        if (this.isNativePlatform) {
          this.file.writeFile(cordova.file.dataDirectory, fileList[0].name, fileList[0], { replace: true }).then((fileEntry) => {
            this.others.push({
              fileKey: "file",
              fileName: fileList[0].name,
              chunkedMode: false,
              mimeType: "multipart/form-data",
              params : {'file': fileList[0].name}
            });
            this.others.push(cordova.file.dataDirectory + fileList[0].name);
          }).catch((err) => {
            alert("Não foi possível anexar o arquivo.");
          });
        } else {
          this.fileFormData.append('file', fileList[0]);
        }
      }
  }

  public addFile() {
    this.fileInput.nativeElement.click();
  }

  public closeCalculoSalario() {
    this.events.publish('page:closePage', 'CalculoSalarioPage');
  }

  public showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });

    this.loading.present();
  }

  public hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

}
