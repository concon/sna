import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NovoCalculoSalarioPage } from './novo-calculo-salario';

@NgModule({
  declarations: [
    NovoCalculoSalarioPage,
  ],
  imports: [
    IonicPageModule.forChild(NovoCalculoSalarioPage),
  ],
})
export class NovoCalculoSalarioPageModule {}
