import { Component } from '@angular/core';
import { Platform, IonicPage, NavParams, LoadingController, Events } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { WageCalculationUnitModel } from '../../app/models/wage-calculation-unit-model';

@IonicPage()
@Component({
  selector: 'page-calculo-salario',
  templateUrl: 'calculo-salario.html',
})
export class CalculoSalarioPage {

	private loading;
  private plannedWageCalculation: WageCalculationUnitModel;
  private executedWageCalculation: WageCalculationUnitModel;

  constructor(public platform: Platform,
              public navParams: NavParams,
              public api: ApiProvider,
              public loadingCtrl: LoadingController,
              private events: Events) {
    this.plannedWageCalculation = this.navParams.get('plannedWageCalculation');
    this.executedWageCalculation = this.navParams.get('executedWageCalculation');
    this.closeNovoCalculoSalario();
  }

  ngAfterViewInit() {

  }

  public recalculatePlanned() {
  	this.events.publish('page:open', 'NovoCalculoSalarioPage', null);
  }

  public openExecutedFile() {
    if (this.executedWageCalculation && this.executedWageCalculation.file) {
      this.openFile(this.executedWageCalculation.file);
    }
  }

  public openPlannedFile() {
    if (this.plannedWageCalculation && this.plannedWageCalculation.file) {
      this.openFile(this.plannedWageCalculation.file);
    }
  }

  public openFile(urlFile) {
    this.events.publish('pdf:open', 'PrivateFile', { targetUrl: urlFile });
  }

  public closeNovoCalculoSalario() {
    this.events.publish('page:closePage', 'NovoCalculoSalarioPage');
  }

  public showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });

    this.loading.present();
  }

  public hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

}
