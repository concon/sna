import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CalculoSalarioPage } from './calculo-salario';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    CalculoSalarioPage,
  ],
  imports: [
    IonicPageModule.forChild(CalculoSalarioPage),
    PipesModule
  ],
})
export class CalculoSalarioPageModule {}
