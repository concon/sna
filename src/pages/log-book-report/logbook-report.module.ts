import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import {LogbookReportPage} from './logbook-report';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    LogbookReportPage,
  ],
  imports: [
    IonicPageModule.forChild(LogbookReportPage),
    PipesModule
  ],
})
export class LogbookReportPageModule {}
