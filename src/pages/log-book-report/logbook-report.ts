import { Component } from '@angular/core';
import {Platform, IonicPage, LoadingController, Events} from 'ionic-angular';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiProvider } from '../../providers/api/api';

@IonicPage()
@Component({
  selector: 'page-logbook-report',
  templateUrl: 'logbook-report.html',
})
export class LogbookReportPage {

  public specialtyList = [];
  public specialtyScheduleList = [];
  public selectedSchedule;
  private loading;

  public specialtyScheduleForm: FormGroup;
  public hasFormError = false;

  constructor(public platform: Platform,
              public api: ApiProvider,
              public loadingCtrl: LoadingController,
              private events: Events) {
    this.specialtyScheduleForm = new FormGroup({
      selectedSpecialty: new FormControl('', [Validators.required]),
      selectedSchedule: new FormControl('', [Validators.required])
    });
  }

  ngAfterViewInit() {
    this.platform.ready().then(() => {
      this.loadSpecialties();
    });
  }

  ionViewWillLeave() {
    //this.hideLoading();
  }

  public loadSpecialties() {
    this.showLoading();
    this.api.getSpecialties().subscribe(
      specialties => {
        this.specialtyList = specialties;
        this.hideLoading();
      }, errorMessage => {
        alert(errorMessage);
        this.hideLoading();
      }
    );
  }

  public getDate(dateArray) {
    var year = dateArray[0];
    var month = dateArray[1];
    var day = dateArray[2];
    var hours = dateArray[3];
    var minutes = dateArray[4];
    //var seconds = dateArray[5];

    return day + "/" + month + "/" + year + " - " +  hours + ":" + minutes;
    // dd/MM/yyyy - HH:mm
  }

  public loadSpecialtySchedules(specialtyId) {
    this.showLoading();
    this.api.getSpecialtySchedules(specialtyId).subscribe(
      specialtySchedules => {
        this.specialtyScheduleList = specialtySchedules;
        this.hideLoading();
      }, errorMessage => {
        alert(errorMessage);
        this.hideLoading();
      }
    );
  }

  public doSchedule() {

    if(this.specialtyScheduleForm.value.selectedSchedule == ""){
      alert("É obrigatório informar o período");
    }else{
      var listData = this.specialtyScheduleForm.value.selectedSchedule.split("-");



      this.showLoading();
      this.api.getLogbookReportSummary(listData[1], listData[0]).subscribe(
        specialtySchedules => {
          this.hideLoading();
          this.events.publish('pdf:open', 'PrivateFile', { targetUrl: specialtySchedules.url });
        }, errorMessage => {
          alert(errorMessage);
          this.hideLoading();
        }
      );
    }


  }

  public showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });

    this.loading.present();
  }

  public hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  public onSpecialtySelectionChange(selectedOption) {
    this.specialtyScheduleList = [];
    this.loadSpecialtySchedules(selectedOption);
  }

  public onScheduleSelectionChange(selectedOption) {
    this.selectedSchedule = selectedOption;
  }

  public openFile(urlFile) {
    this.events.publish('pdf:open', 'PrivateFile', { targetUrl: urlFile });
  }

}
