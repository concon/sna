import { Component } from '@angular/core';
import { IonicPage, NavParams, LoadingController, NavController, Events } from 'ionic-angular';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ApiProvider } from '../../providers/api/api';
import { SessionProvider } from '../../providers/session/session';
import { LogbookModel } from '../../app/models/logbook-model';
import { DatePipe } from '@angular/common';
import {AirlineModel} from "../../app/models/airline-model";

@IonicPage()
@Component({
  selector: 'page-logbook-manual',
  templateUrl: 'logbook-manual.html',
})
export class LogbookManualPage {

  public editMode = false;
  public logbook: LogbookModel;
	private loading;
  public registrationForm: FormGroup;
  public hasFormError = false;
  public airportsFrom = [];
  public airportsTo = [];
  public airportFromIcao = "";
  public airportToIcao = "";
  public airportFrom = "";
  public airportTo = "";
  public chegadaLocal = "";
  public saidaLocal = "";
  public chegadaUTC = "";
  public saidaUTC = "";
  public atualInst = "";
  public pic = "";
  public sic = "";
  public offDuttyLocal = "";
  public offDuttyUTC = "";
  public onDuttyLocal = "";
  public onDuttyUTC = "";
  public horasTotal = "";
  public horasNoturnas = "";
  public day = "";
  public day_ldg = "";
  public night = "";
  public night_ldg = "";

  constructor(public navParams: NavParams,
              public navCtrl: NavController,
  						public api: ApiProvider,
              public session: SessionProvider,
              public events: Events,
              public loadingCtrl: LoadingController,
              private datePipe: DatePipe) {

    if (this.navParams.get('logbookData')) {
        this.editMode = true;
        this.logbook = this.navParams.get('logbookData');

      let dateValue = '';
      if (this.logbook.data) {
        var data = new Date();
        data.setDate(Number(this.logbook.data[2]));
        data.setMonth(Number(this.logbook.data[1]) - 1);
        data.setFullYear(Number(this.logbook.data[0]));
        dateValue = data.toISOString();
      }

      this.horasTotal = this.transformDurationTime(this.logbook.horasTotal);
      this.horasNoturnas = this.transformDurationTime(this.logbook.horasNoturnas);
      this.day = this.transformDurationTime(this.logbook.day);
      this.day_ldg = this.transformDurationTime(this.logbook.dayLdg);
      this.night = this.transformDurationTime(this.logbook.night);
      this.night_ldg = this.transformDurationTime(this.logbook.nightLdg);
      this.chegadaLocal = this.transformDateTime(this.logbook.chegadaLocal);
      this.saidaLocal = this.transformDateTime(this.logbook.saidaLocal);
      this.chegadaUTC = this.transformDateTime(this.logbook.chegadaUTC);
      this.saidaUTC = this.transformDateTime(this.logbook.saidaUTC);
      this.atualInst = this.transformDateTime(this.logbook.atualInst);
      this.pic = this.transformDateTime(this.logbook.pic);
      this.sic = this.transformDateTime(this.logbook.sic);
      this.offDuttyLocal = this.transformDateTime(this.logbook.offDuttyLocal);
      this.offDuttyUTC = this.transformDateTime(this.logbook.offDuttyUTC);
      this.onDuttyLocal = this.transformDateTime(this.logbook.onDuttyLocal);
      this.onDuttyUTC = this.transformDateTime(this.logbook.onDuttyUTC);

      this.registrationForm = new FormGroup({
        aeronaveId: new FormControl(this.logbook.aeronaveId, []),
        aeronaveTipo: new FormControl(this.logbook.aeronaveTipo, []),
        aeroportoChegada: new FormControl(this.logbook.aeroportoChegada, []),
        aeroportoSaida: new FormControl(this.logbook.aeroportoSaida, []),
        atualInst: new FormControl(this.logbook.atualInst, []),
        chegadaLocal: new FormControl(this.logbook.chegadaLocal, []),
        data: new FormControl(dateValue, [Validators.required]),
        day: new FormControl(this.logbook.day, []),
        day_ldg: new FormControl(this.logbook.dayLdg, []),
        horasNoturnas: new FormControl(this.logbook.horasNoturnas, []),
        horasTotal: new FormControl(this.logbook.horasTotal, []),
        id: new FormControl(this.logbook.id, [Validators.required]),
        night: new FormControl(this.logbook.night, []),
        night_ldg: new FormControl(this.logbook.nightLdg, []),
        offDuttyLocal: new FormControl(this.logbook.offDuttyLocal, []),
        offDuttyUTC: new FormControl(this.logbook.offDuttyUTC, []),
        onDuttyLocal: new FormControl(this.logbook.onDuttyLocal, []),
        onDuttyUTC: new FormControl(this.logbook.onDuttyUTC, []),
        pic: new FormControl(this.logbook.pic, []),
        pic_p1: new FormControl(this.logbook.picP1, []),
        remarks: new FormControl(this.logbook.remarks, []),
        saidaLocal: new FormControl(this.logbook.saidaLocal, []),
        sic: new FormControl(this.logbook.sic, []),
        sic_p2: new FormControl(this.logbook.sicP2, []),
        voo: new FormControl(this.logbook.voo, []),
        vooPiloto: new FormControl(this.logbook.vooPiloto, []),
        user: new FormControl('', [Validators.required])
      });

      this.airportTo = this.logbook.aeroportoChegada;
      this.airportFrom = this.logbook.aeroportoSaida;

      console.log(this.airportTo)
      console.log(this.airportFrom)

    } else {
      this.registrationForm = new FormGroup({
        aeronaveId: new FormControl('', []),
        aeronaveTipo: new FormControl('', []),
        aeroportoChegada: new FormControl('', []),
        aeroportoSaida: new FormControl('', []),
        atualInst: new FormControl('', []),
        chegadaLocal: new FormControl('', []),
        data: new FormControl('', [Validators.required]),
        day: new FormControl('', []),
        day_ldg: new FormControl('', []),
        horasNoturnas: new FormControl('', []),
        horasTotal: new FormControl('', []),
        night: new FormControl('', []),
        night_ldg: new FormControl('', []),
        offDuttyLocal: new FormControl('', []),
        offDuttyUTC: new FormControl('', []),
        onDuttyLocal: new FormControl('', []),
        onDuttyUTC: new FormControl('', []),
        pic: new FormControl('', []),
        pic_p1: new FormControl('', []),
        remarks: new FormControl('', []),
        saidaLocal: new FormControl('', []),
        sic: new FormControl('', []),
        sic_p2: new FormControl('', []),
        voo: new FormControl('', []),
        vooPiloto: new FormControl(false, []),
        user: new FormControl('', [Validators.required])
      });
    }

    this.session.getUserJson().then(user => {
      this.registrationForm.controls["user"].setValue(user);
    });
  }

  public doRequest() {
    if (this.editMode) {
      this.doUpdate();
    } else {
      this.doRegistration();
    }
  }

  private doUpdate() {
    this.hasFormError = !this.registrationForm.valid;
    if (this.registrationForm.valid) {
      this.showLoading();
      this.api.doLogbookUpdate(this.registrationForm.value).subscribe(
        logbookData => {
          if (this.editMode) {
              this.navCtrl.getPrevious().data.updatedData = logbookData;
          }
          alert("Dados atualizados com sucesso!");
          this.closePage();
          this.hideLoading();
        }, message => {
          alert(message);
          this.hideLoading();
        }
      );
    }
  }

  public doRegistration() {
    this.hasFormError = !this.registrationForm.valid;
    if (this.registrationForm.valid) {
      this.showLoading();
      this.api.doNewLogbook(this.registrationForm.value).subscribe(
        logbookData => {
          alert("Cadastro realizado com sucesso!");
          this.closePage();
          this.hideLoading();
        }, message => {
          alert(message);
          this.hideLoading();
        }
      );
    }
  }

  public onKeyUp(event: any, fieldName) {
    // Remove all, but numbers and colons
    let newValue = event.target.value.replace(/[^0-9:]/g, "");

    // Slice duplicates colons, or if it is alone
    if ((newValue.match(/:/g) || []).length > 1 || (newValue == ':')) {
      newValue = newValue.replace(/[^0-9]/g, "");
    }

    // Mapping the time number to apply the rules of minutes
    if ((newValue.match(/:/g) || []).length == 1) {
      var time = newValue.split(':').map(el => {
        return el
      });

      if (time.length == 2) {
        var minutes = time[1];
        if (minutes && (minutes > 59 || minutes.toString().split('').length > 2)) {
          newValue = newValue.slice(0, -1);
        }
      }
    }

    // save the new value to field
    this[fieldName] = newValue;

    // update the form with new value in milliseconds
    var hms = newValue;
    var b = hms.split(':').map(el => {
      let n = Number(el);
      return n === 0 ? n : n || el;
    });
    var milliseconds = 0;
    if (b.length > 0) {
      milliseconds += (b[0] * 3600000);
      if (b.length == 2) {
        milliseconds += (b[1] * 60000)
      }
    }
    this.registrationForm.controls[fieldName].setValue(milliseconds);
  }

  public edit(data, fieldName, fieldType, additional?: string) {
    if (fieldType == "time") {
      this.registrationForm.controls[fieldName].setValue(Date.parse('2019-05-05T' + this.getTextTime(data.hour) + ':' + this.getTextTime(data.minute) + ':00.000-03:00'));
    } else {
      this.registrationForm.controls[fieldName].setValue(data[fieldName]);
    }
  }

  public closePage() {
    this.events.publish('page:back');
  }

  public showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Carregando...'
    });

    this.loading.present();
  }

  public hideLoading() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  private transformDurationTime(duration) {
    let minutes = Math.floor((duration / (1000 * 60)) % 60);
    let hours = Math.floor((duration / (1000 * 60 * 60)));
    let formattedTime = this.getTextTime(hours) + ":" + this.getTextTime(minutes);

    return formattedTime;
  }

  private transformDateTime(date) {
    return this.datePipe.transform(date, 'HH:mm');
  }

  private getTextTime(time) {
  	return (time < 10 ? '0' : '') + time;
  }


  public findAirportFrom(text) {
    this.api.getLogbookAirposts(text).subscribe(
      logbookAirportData => {
        if(logbookAirportData.length == 1){
          this.airportFrom = logbookAirportData[0].icao;
          this.airportsFrom = [];
        }else{
          this.airportsFrom = logbookAirportData;
        }

      }, message => {
        alert(message);
        this.hideLoading();
      }
    );
  }

  public selectFrom(item){
    this.airportFrom = item.icao;
    this.airportsFrom = [];
  }

  public findAirportTo(text) {
    this.api.getLogbookAirposts(text).subscribe(
      logbookAirportData => {
        if(logbookAirportData.length == 1){
          this.airportTo = logbookAirportData[0].icao;
          this.airportsTo = [];
        }else{
          this.airportsTo = logbookAirportData;
        }

      }, message => {
        alert(message);
        this.hideLoading();
      }
    );
  }

  public selectTo(item){
    this.airportTo = item.icao;
    this.airportsTo = [];
  }


}
