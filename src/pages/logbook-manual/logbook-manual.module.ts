import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LogbookManualPage } from './logbook-manual';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    LogbookManualPage,
  ],
  imports: [
    IonicPageModule.forChild(LogbookManualPage),
    PipesModule
  ],
})
export class LogbookManualPageModule {}
