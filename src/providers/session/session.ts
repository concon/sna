import { Storage } from "@ionic/storage";

import { Injectable } from '@angular/core';

import { SessionModel } from '../../app/models/session-model';

import { UserModel } from '../../app/models/user-model';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { JsonConvert } from "json2typescript";

import CryptoJS from 'crypto-js';

@Injectable()
export class SessionProvider {

  private sessionData: SessionModel;
  private parole: string = "sp_@cr!_tl";
  public sessionDataObservable;

	constructor(public storage: Storage) {
    this.sessionDataObservable = new BehaviorSubject<SessionModel>(this.sessionData);
    this.notifyObserversAboutSessionUpdate();
  }

  encrypt(data, key) {
    try {
      if (data) {
        return CryptoJS.AES.encrypt(JSON.stringify(this.parseModelToJson(data)), key).toString();
      }
    } catch (error) {
      this.remove();
    }
    
    return data;
  }

  decrypt(data, key) {
    try {
      if (data) {
        let bytes = CryptoJS.AES.decrypt(data, key);
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      }
    } catch (error) {
      this.remove();
    }
      
    return data;
  }

  create(session: SessionModel) {
    return this.storage.set('session', this.encrypt(session, this.parole)).then(res => {
      this.notifyObserversAboutSessionUpdate();
    });
  }

  update(session: SessionModel) {
    return this.storage.set('session', this.encrypt(session, this.parole)).then(res => {
      this.notifyObserversAboutSessionUpdate();
    });
  }

  remove() {
    this.storage.remove('session').then(res => {
      this.notifyObserversAboutSessionUpdate();
    });
  }

  get(): any {
    return this.storage.get('session').then(res => {
      return this.parseModelFromJson(this.decrypt(res, this.parole), SessionModel);
    });
  }

  exist() {
    return this.get().then(res => {
      return (res)? true : false;
    });
  }

  getToken(): any {
    return this.get().then(sessionModel => {
      if (sessionModel && sessionModel.token) {
        return sessionModel.token;
      } else {
        return null;
      }
    });
  }

  getUser(): any {
    return this.get().then(sessionModel => {
      if (sessionModel && sessionModel.user) {
        return this.parseModelFromJson(sessionModel.user, UserModel);
      } else {
        return null;
      }
    }); 
  }

  getUserJson(): any {
    return this.get().then(sessionModel => {
      if (sessionModel && sessionModel.user) {
        return sessionModel.user;
      } else {
        return null;
      }
    }); 
  }

  public notifyObserversAboutSessionUpdate() {
    return this.get().then(sessionModel => {
      this.sessionDataObservable.next(sessionModel);
    });
  }

  public updateUserData(userData) {
    return this.get().then(sessionModel => {
      if (sessionModel) {
        sessionModel.user = userData;
        this.update(sessionModel);
      }
    });
  }

  private parseModelFromJson(res, model) {
    let result = null;
    try {
      let jsonConverter: JsonConvert = new JsonConvert();
      result = jsonConverter.deserialize(res, model);
    } catch (error) {
      
    }

    return result;
  }

  private parseModelToJson(res) {
    let result = null;
    try {
      let jsonConverter: JsonConvert = new JsonConvert();
      result = jsonConverter.serialize(res);
    } catch (error) {

    }

    return result;
  }

}
