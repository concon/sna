import { Platform, Events } from 'ionic-angular';
import { HttpProvider } from '../http/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';
import { JsonConvert } from "json2typescript";
import * as $ from 'jquery'

import { SessionProvider } from '../../providers/session/session';

import { AirlineModel } from '../../app/models/airline-model';
import { CargoModel } from '../../app/models/cargo-model';
import { ArticleModel } from '../../app/models/article-model';
import { CategoryModel } from '../../app/models/category-model';
import { LogbookModel } from '../../app/models/logbook-model';
import { LogbookSummaryModel } from "../../app/models/logbook-summary-model";
import { RequestModel } from '../../app/models/request-model';
import { SessionModel } from '../../app/models/session-model';
import { SpecialtyModel } from '../../app/models/specialty-model';
import { SpecialtyScheduleModel } from '../../app/models/specialty-schedule-model';
import { UserModel } from '../../app/models/user-model';
import { VirtualCardModel } from '../../app/models/virtual-card-model';
import { WageCalculationUnitModel } from '../../app/models/wage-calculation-unit-model';
import {LogbookReportModel} from "../../app/models/logbook-report-model";
import {LogbookAirportModel} from "../../app/models/logbook-airport";

@Injectable()
export class ApiProvider {

  constructor(private platform: Platform,
              private httpProvider: HttpProvider,
              private session: SessionProvider,
              private events: Events) { }

  public doLogin(params) {
    return this.httpProvider.http.post('http://54.161.240.83:8080/snaapp/login?cpf=' + params.cpf + '&senha=' + params.senha, {}, {})
      .pipe(
        map(res => this.parseModelFromJson(res, SessionModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public doChangePassword(params) {
    return this.httpProvider.http.postWithAuthHeader('http://54.161.240.83:8080/snaapp/auth/password/change', params, {})
      .pipe(
        map(res => this.parseModelFromJson(res, UserModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public doChangeImage(params, others) {
    return this.httpProvider.http.postMultipartWithAuthHeader('http://54.161.240.83:8080/snaapp/auth/user/perfil/image', params, {}, others)
      .pipe(
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public doUserUpdate(params) {
    return this.httpProvider.http.putWithAuthHeader('http://54.161.240.83:8080/snaapp/user', params, {})
      .pipe(
        map(res => this.parseModelFromJson(res, UserModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public doUserRegistration(params) {
    return this.httpProvider.http.post('http://54.161.240.83:8080/snaapp/user', params, {})
      .pipe(
        map(res => this.parseModelFromJson(res, SessionModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getUserInfo() {
    return this.httpProvider.http.getWithAuthHeader('http://54.161.240.83:8080/snaapp/auth/user/info', {}, {})
      .pipe(
        map(res => this.parseModelFromJson(res, UserModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getLogbooks(page, size) {
    return this.httpProvider.http.getWithAuthHeader('http://54.161.240.83:8080/snaapp/auth/logbook?page=' + page + '&size=' + size, {}, {})
      .pipe(
        map(res => this.parseModelFromJson(res, LogbookModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getLogbookSummary() {
    return this.httpProvider.http.getWithAuthHeader('http://54.161.240.83:8080/snaapp/auth/logbook/resumo', {}, {})
      .pipe(
        map(res => this.parseModelFromJson(res, LogbookSummaryModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getLogbookReportSummary(mes, ano) {
    return this.httpProvider.http.getWithAuthHeader('http://54.161.240.83:8080/snaapp/auth/logbook/report?mes='+mes+'&ano='+ano, {}, {})
      .pipe(
        map(res => this.parseModelFromJson(res, LogbookReportModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getLogbookAirposts(text) {
    return this.httpProvider.http.getWithAuthHeader('http://54.161.240.83:8080/snaapp/auth/logbook/airport?text='+text, {}, {})
      .pipe(
        map(res => this.parseModelFromJson(res, LogbookAirportModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public doLogbookUpdate(params) {
    return this.httpProvider.http.putWithAuthHeader('http://54.161.240.83:8080/snaapp/auth/logbook', params, {})
      .pipe(
        map(res => this.parseModelFromJson(res, LogbookModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public doLogbookDelete(id) {
    return this.httpProvider.http.deleteWithAuthHeader('http://54.161.240.83:8080/snaapp/auth/logbook/'+id, {}, {})
      .pipe(
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public doNewLogbook(params) {
    return this.httpProvider.http.postWithAuthHeader('http://54.161.240.83:8080/snaapp/auth/logbook', params, {})
      .pipe(
        map(res => this.parseModelFromJson(res, LogbookModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getVirtualCardData(cpf) {
    return this.httpProvider.http.getWithAuthHeader('http://aer.completo.com.br/carteirinha-srv/api/encontrar-pessoa-cpf/' + cpf)
      .pipe(
        map(res => this.parseModelFromJson(res["pessoa-fisica"], VirtualCardModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public doSearchNews(searchText) {
    return this.httpProvider.http.get('https://tjyllgsih7.execute-api.us-east-1.amazonaws.com/HML/v1/search?texto=' + searchText)
      .pipe(
        map(res => this.parseModelFromJson(res["articles"], ArticleModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getNewById(newId) {
    return this.httpProvider.http.get('http://www.aeronautas.org.br/index.php?option=com_jbackend&action=get&module=content&resource=articles&id=' + newId)
      .pipe(
        map(res => this.parseModelFromJson(res, ArticleModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getLatestNews() {
    return this.httpProvider.http.get('http://www.aeronautas.org.br/index.php?option=com_jbackend&action=get&module=content&resource=articles&catid=8&limit=3&orderby=created&orderdir=desc')
      .pipe(
        map(res => this.parseModelFromJson(res["articles"], ArticleModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getNewsWithOffset(offset, quantity) {
    return this.httpProvider.http.get('http://www.aeronautas.org.br/index.php?option=com_jbackend&action=get&module=content&resource=articles&catid=8&limit=' + quantity + '&orderby=created&orderdir=desc&offset=' + offset)
      .pipe(
        map(res => this.parseModelFromJson(res["articles"], ArticleModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getAssembliesWithOffset(offset, quantity) {
    return this.httpProvider.http.get('http://www.aeronautas.org.br/index.php?option=com_jbackend&action=get&module=content&resource=articles&catid=67&limit=' + quantity + '&orderby=created&orderdir=desc&offset=' + offset)
      .pipe(
        map(res => this.parseModelFromJson(res["articles"], ArticleModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getAcoesColetivasWithOffset(offset, quantity) {
    return this.httpProvider.http.get('http://www.aeronautas.org.br/index.php?option=com_jbackend&action=get&module=content&resource=articles&catid=481&limit=' + quantity + '&orderby=created&orderdir=desc&offset=' + offset)
      .pipe(
        map(res => this.parseModelFromJson(res["articles"], ArticleModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getPartnershipsWithOffset(offset, quantity, categoryId) {
    return this.httpProvider.http.get('http://www.aeronautas.org.br/index.php?option=com_jbackend&view=request&action=get&module=content&resource=articles&catid=' + categoryId + '&limit=' + quantity + '&orderby=created&orderdir=desc&offset=' + offset)
      .pipe(
        map(res => this.parseModelFromJson(res["articles"], ArticleModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getJobAndOpportunitiesWithOffset(offset, quantity) {
    return this.httpProvider.http.get('http://www.aeronautas.org.br/index.php?option=com_jbackend&action=get&module=content&resource=articles&catid=58&limit=' + quantity + '&orderby=created&orderdir=desc&offset=' + offset)
      .pipe(
        map(res => this.parseModelFromJson(res["articles"], ArticleModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getStateList() {
    return this.httpProvider.http.get('http://www.aeronautas.org.br/index.php?option=com_jbackend&action=get&module=content&resource=categories&rootid=17&recursive=false')
      .pipe(
        map(res => this.parseModelFromJson(res["categories"], CategoryModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getTypeList(stateId) {
    return this.httpProvider.http.get('http://www.aeronautas.org.br/index.php?option=com_jbackend&action=get&module=content&resource=categories&rootid=' + stateId + '&recursive=false')
      .pipe(
        map(res => this.parseModelFromJson(res["categories"], CategoryModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getSpecialties() {
    return this.httpProvider.http.getWithAuthHeader('http://54.161.240.83:8080/snaapp/auth/consulta/especialidades')
      .pipe(
        map(res => this.parseModelFromJson(res, SpecialtyModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getAirlineList() {
    return this.httpProvider.http.getWithAuthHeader('http://54.161.240.83:8080/snaapp/auth/companhia')
      .pipe(
        map(res => this.parseModelFromJson(res, AirlineModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getCargos() {
    return this.httpProvider.http.getWithAuthHeader('http://54.161.240.83:8080/snaapp/auth/user/cargos')
      .pipe(
        map(res => this.parseModelFromJson(res, CargoModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getSpecialtySchedules(specialtyId) {
    return this.httpProvider.http.getWithAuthHeader('http://54.161.240.83:8080/snaapp/auth/consulta/horarios?especialidade=' + specialtyId)
      .pipe(
        map(res => this.parseModelFromJson(res, SpecialtyScheduleModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getWageCalculationList(page, size) {
    return this.httpProvider.http.getWithAuthHeader('http://54.161.240.83:8080/snaapp/auth/calculosalario/list?page=' + page + '&size=' + size)
      .pipe(
        map(res => this.parseModelFromJson(res, WageCalculationUnitModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public doSpecialtySchedule(scheduleId) {
    return this.httpProvider.http.postWithAuthHeader('http://54.161.240.83:8080/snaapp/auth/consulta/horarios/confirm?horario=' + scheduleId)
      .pipe(
        map(res => this.parseModelFromJson(res, SpecialtyScheduleModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getRequests(requestType) {
    return this.httpProvider.http.getWithAuthHeader('http://54.161.240.83:8080/snaapp/auth/atendimento/tipo/' + requestType)
      .pipe(
        map(res => this.parseModelFromJson(res, RequestModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public doCloseRequest(id) {
    return this.httpProvider.http.putWithAuthHeader('http://54.161.240.83:8080/snaapp/auth/atendimento/' + id + '/status/FECHADO', {}, {})
      .pipe(
        map(res => this.parseModelFromJson(res, RequestModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getRequestDetails(id) {
    return this.httpProvider.http.getWithAuthHeader('http://54.161.240.83:8080/snaapp/auth/atendimento/' + id)
      .pipe(
        map(res => this.parseModelFromJson(res, RequestModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public doNewRequest(params) {
    return this.httpProvider.http.postWithAuthHeader('http://54.161.240.83:8080/snaapp/auth/atendimento', params, {})
      .pipe(
        map(res => this.parseModelFromJson(res, RequestModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public doAnswerRequest(params) {
    return this.httpProvider.http.postWithAuthHeader('http://54.161.240.83:8080/snaapp/auth/atendimento/chat', params, {})
      .pipe(
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public doNewRequestUploadFile(requestId, body, others, nome) {
    return this.httpProvider.http.postMultipartWithAuthHeader('http://54.161.240.83:8080/snaapp/auth/atendimento/anexo?atendimento=' + requestId+"&nome="+nome, body, {}, others)
      .pipe(
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public doRemoveFileFromRequest(id) {
    return this.httpProvider.http.deleteWithAuthHeader('http://54.161.240.83:8080/snaapp/auth/atendimento/anexo/' + id, {}, {})
      .pipe(
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public doNewWageCalculationUploadFile(rangeType, logbookIncluded, body, others) {
    return this.httpProvider.http.postMultipartWithAuthHeader('http://54.161.240.83:8080/snaapp/auth/calculosalario/tipo/' + rangeType + '/logbook/' + logbookIncluded, body, {}, others)
      .pipe(
        map(res => this.parseModelFromJson(res, WageCalculationUnitModel)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getFreePassGolLoginUrl(cpf, anacCode) {
    return this.httpProvider.http.getHtml('https://passelivre.voegol.com.br/')
      .pipe(
        map(res => this.getFreePassGolUrl(res, cpf, anacCode)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getFreePassAzulLoginUrl(cpf, anacCode) {
    return this.httpProvider.http.getHtml('https://apps.voeazul.com.br/passelivreintegrado/Login.aspx')
      .pipe(
        map(res => this.getFreePassAzulUrl(res, cpf, anacCode)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  public getFreePassAviancaLoginUrl(cpf, anacCode) {
    return this.httpProvider.http.getHtml('https://passelivre.avianca.com.br/01/Login.aspx')
      .pipe(
        map(res => this.getFreePassAviancaUrl(res, cpf, anacCode)),
        catchError((err, caught) => {  throw this.parseError(err) })
      );
  }

  private getFreePassGolUrl(responseHtml, cpf, anacCode) {
    var viewstageIndex = this.platform.is('mobileweb') || this.platform.is('android') ? 3 : 1;
    var generatorIndex = this.platform.is('mobileweb') || this.platform.is('android') ? 4 : 2;
    var eventvalidationIndex = this.platform.is('mobileweb') || this.platform.is('android') ? 5 : 3;

    var pageAsJquery = $('<div/>').html(responseHtml).contents();
    var viewstage = encodeURIComponent(pageAsJquery[24][viewstageIndex].value);
    var generator = encodeURIComponent(pageAsJquery[24][generatorIndex].value);
    var eventvalidation = encodeURIComponent(pageAsJquery[24][eventvalidationIndex].value);

    return 'https://passelivre.voegol.com.br?__VIEWSTATE=' + viewstage + '&__VIEWSTATEGENERATOR=' + generator + '&__EVENTVALIDATION=' + eventvalidation + '&ctl00%24ContentPlaceHolder1%24txtTaxPayerId=' + cpf + '&ctl00%24ContentPlaceHolder1%24txtANACCode=' + anacCode + '&__ASYNCPOST=true&ctl00%24ContentPlaceHolder1%24btnSubmit=Enviar';
  }

  private getFreePassAzulUrl(responseHtml, cpf, anacCode) {

    var viewstageIndex = this.platform.is('mobileweb') || this.platform.is('android') ? 3 : 1;
    var generatorIndex = this.platform.is('mobileweb') || this.platform.is('android') ? 4 : 2;
    var eventvalidationIndex = this.platform.is('mobileweb') || this.platform.is('android') ? 5 : 3;

    var pageAsJquery = $('<div/>').html(responseHtml).contents();

    var viewstage = encodeURIComponent(pageAsJquery[11][viewstageIndex].value);
    var generator = encodeURIComponent(pageAsJquery[11][generatorIndex].value);
    var eventvalidation = encodeURIComponent(pageAsJquery[11][eventvalidationIndex].value);

    return 'https://apps.voeazul.com.br/passelivreintegrado/Login.aspx?__VIEWSTATE='+viewstage+'&__VIEWSTATEGENERATOR='+generator+'&__EVENTVALIDATION='+eventvalidation+'&ctl00%24ContentPlaceHolder1%24txtTaxPayerId='+cpf+'&ctl00%24ContentPlaceHolder1%24txtAnacCode='+anacCode+'&__ASYNCPOST=true&ctl00%24ContentPlaceHolder1%24btnSubmit=Enviar';
  }



  private getFreePassAviancaUrl(responseHtml, cpf, anacCode) {

    var viewstageIndex = this.platform.is('mobileweb') || this.platform.is('android') ? 2 : 1;
    var generatorIndex = this.platform.is('mobileweb') || this.platform.is('android') ? 3 : 2;

    var pageAsJquery = $('<div/>').html(responseHtml).contents();
    var viewstage = encodeURIComponent(pageAsJquery[20][viewstageIndex].value);
    var generator = encodeURIComponent(pageAsJquery[20][generatorIndex].value);

    return 'https://passelivre.avianca.com.br/01/Login.aspx?__VIEWSTATE=' + viewstage + '&__VIEWSTATEGENERATOR=' + generator + '&ctl00%24ContentPlaceHolder1%24txtTaxPayerId=' + cpf + '&ctl00%24ContentPlaceHolder1%24txtANACCode=' + anacCode + '&ctl00%24ContentPlaceHolder1%24btnSubmit=Enviar';
  }

  public getExampleMock() {
    return new Observable((observer) => {
      setTimeout(() => {
        let exampleList = [
          {
            "a": "value_a",
            "b": "value_b"
          },
          {
            "c": "value_c",
            "d": "value_d"
          }
        ];
        observer.next(exampleList);
      }, 3000);
    });
  }


  private parseModelFromJson(res, model) {
    let jsonConverter: JsonConvert = new JsonConvert();
    return jsonConverter.deserialize(res, model);
  }

  private parseError(err) {
    console.log(err);
    try {
      err = JSON.parse(err)
    } catch (error) { /* Do nothing */ }
    try {
      err.error = JSON.parse(err.error);
    } catch (error) { /* Do nothing */ }

    if (err.error && err.error["access-denied"] && err.error.cause && err.error.cause == "AUTHORIZATION_FAILURE") {
      this.session.remove();
      this.goBackToRoot();
      return new Error("Sessão expirada!").message;
    } else if (err.error && err.error.message) {
      return new Error(err.error.message).message;
    } else if (err.error && err.error.detail) {
      return new Error(err.error.detail).message;
    } else {
      return new Error("Não foi possível processar sua requisição, tente novamente.").message;
    }
  }

  public goBackToRoot() {
    this.events.publish('page:backToRoot');
  }

}
