import { Injectable } from '@angular/core';

import { SessionProvider } from '../../providers/session/session';

@Injectable()
export class AuthGuardProvider {

	private restrictedPages = [
		// Pages
		'AgendarConsultaPage',
		'CalculoSalarioPage',
		'CarteirinhaVirtualPage',
		'ListaSalarioPage',
		'ListaSolicitacoesPage',
		'PasseLivrePage',
		'PerfilPage',
		'SolicitarAtendimentoPage',
		'NovoCalculoSalarioPage',
		'LogBookListaPage',
		'DetalhesSolicitacoesPage',

		// InApp Browser
		'PrivateFile',
		'Avianca',
		'Azul',
		'Gol'
	];

	private additionalProfileDataRequired = [
		// Pages
		'CalculoSalarioPage',
		'CarteirinhaVirtualPage',
		'ListaSalarioPage',
		'NovoCalculoSalarioPage',
		'PasseLivrePage',

		// InApp Browser
		'Avianca',
		'Azul',
		'Gol'
	];

  constructor(public session: SessionProvider) { }

  public validateAccess(page): Promise<boolean> {
  	return new Promise((resolve, reject) => {
	  	if (this.isAccessAllowed(page)) {
		    resolve(true);
	    } else {
	    	this.session.exist().then((res) => {
	    		resolve(res);
	      })
	      .catch((err) => {
	        reject(false);
	    	});
	    }
  	});
	}

	public validateProfileData(page): Promise<boolean> {
  	return new Promise((resolve, reject) => {
  		if (this.isProfileAdditionalDataNotRequired(page)) {
		    resolve(true);
	    } else {
	    	this.session.getUser().then((userData) => {
	    		let result = (userData.anaccode && userData.companhia)? true : false;
	    		resolve(result);
	      })
	      .catch((err) => {
	        reject(false);
	    	});
	    }
  	});
	}

	private isAccessAllowed(page) {
		var isRestrictedPage = this.restrictedPages.find(function(element) {
			if (page == element) {
				return true;
			}
  		return false;
		});
  	return !isRestrictedPage;
	}

	private isProfileAdditionalDataNotRequired(page) {
		var isProfileAdditionalDataRequired = this.additionalProfileDataRequired.find(function(element) {
			if (page == element) {
				return true;
			}
  		return false;
		});
  	return !isProfileAdditionalDataRequired;
	}

}
