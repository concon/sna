import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';

import { HttpAngularProvider } from '../http-angular/http-angular';
import { HttpNativeProvider } from '../http-native/http-native';

@Injectable()
export class HttpProvider {
  
  public http: HttpNativeProvider | HttpAngularProvider;

  constructor(private platform: Platform,
  						private angularHttp: HttpAngularProvider,
  						private nativeHttp: HttpNativeProvider) {
    this.http = this.platform.is('mobileweb') || this.platform.is('android') ? this.angularHttp : this.nativeHttp;
  }
  
}