import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http';
import { Platform, ToastController } from 'ionic-angular';

import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/fromPromise';

import { SessionProvider } from '../../providers/session/session';

import { Network } from '@ionic-native/network';

import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

@Injectable()
export class HttpNativeProvider {

  private hasNetworkConnection: boolean;

  constructor(private transfer: FileTransfer,
              private file: File,
              public platform: Platform,
              public http: HTTP,
              public session: SessionProvider,
              private network: Network,
              public toastCtrl: ToastController) {

    platform.ready().then(() => {
      // this.network.onConnect().subscribe(() => { this.hasNetworkConnection = true });
      // this.network.onDisconnect().subscribe(() => { this.hasNetworkConnection = false });
      this.http.setDataSerializer('json');
    });
  }

  public get(url: any, params?: any, options: any = {}) {
    let responseData = this.http.get(url, params, {})
      .then(resp => options.responseType == 'text' ? resp.data : JSON.parse(resp.data));

    return Observable.fromPromise(this.getValidResponseData(responseData));
  }

  public getHtml(url: any, params?: any, options: any = {}) {
    let responseData = this.http.get(url, params, {})
      .then(resp => resp.data);

    return Observable.fromPromise(this.getValidResponseData(responseData));
  }

  public post(url: any, params?: any, options: any = {}) {
    let responseData = this.http.post(url, params, { "content-type":"application/json" })
      .then(resp => options.responseType == 'text' ? resp.data : JSON.parse(resp.data));

    return Observable.fromPromise(this.getValidResponseData(responseData));
  }

  public getWithAuthHeader(url: any, params?: any, options: any = {}) {
    return this.getValidResponseData(new Observable((observer) => {
        this.session.getToken().then(token => {
          if (token) {
            let headers = {"Authorization": token, "content-type": "application/json; charset=utf-8"};

            this.http.get(url, params, headers)
            .then(resp => options.responseType == 'text' ? resp.data : JSON.parse(resp.data))
            .then(res => { observer.next(res) }).catch(error => { observer.error(error) });
          } else {
            observer.error("Token exception");
          }
        });
      })
    );
  }

  public deleteWithAuthHeader(url: any, params?: any, options: any = {}) {
    return this.getValidResponseData(new Observable((observer) => {
        this.session.getToken().then(token => {
          if (token) {
            let headers = {"Authorization": token, "content-type": "application/json; charset=utf-8"};

            this.http.delete(url, params, headers)
              .then(resp => options.responseType == 'text' ? resp.data : JSON.parse(resp.data))
              .catch(error => console.log(error))
              .then(res => { observer.next(res) }).catch(error => { observer.error(error) });
          } else {
            observer.error("Token exception");
          }
        });
      })
    );
  }

  public postWithAuthHeader(url: any, params?: any, options: any = {}) {
    return this.getValidResponseData(new Observable((observer) => {
        this.session.getToken().then(token => {
          if (token) {
            let headers = { "Authorization": token, "Accept": "application/json", "Content-type": "application/json" };

            this.http.post(url, params, headers)
            .then(resp => options.responseType == 'text' ? resp.data : JSON.parse(resp.data))
            .then(res => { observer.next(res) }).catch(error => { observer.error(error) });
          } else {
            observer.error("Token exception");
          }
        });
      })
    );
  }

  public putWithAuthHeader(url: any, params?: any, options: any = {}) {
    return this.getValidResponseData(new Observable((observer) => {
        this.session.getToken().then(token => {
          if (token) {
            let headers = { "Authorization": token, "Accept": "application/json", "Content-type": "application/json" };

            this.http.put(url, params, headers)
            .then(resp => options.responseType == 'text' ? resp.data : JSON.parse(resp.data))
            .then(res => { observer.next(res) }).catch(error => { observer.error(error) });
          } else {
            observer.error("Token exception");
          }
        });
      })
    );
  }

  public postMultipartWithAuthHeader(url: any, params?: any, options: any = {}, others?: any) {
    return this.getValidResponseData(new Observable((observer) => {
        this.session.getToken().then(token => {
          if (token) {
            const fileTransfer: FileTransferObject = this.transfer.create();
            let headers = { 'headers' : { "Authorization": token}};
            let uploadOptions = Object.assign(others[0], headers);
            let targetObjectPath = others[1];
            fileTransfer.upload(targetObjectPath, encodeURI(url), uploadOptions)
            .then(resp => {
              if (options.responseType == 'text' || resp.response == '') {
                return resp.response;
              } else {
                return JSON.parse(resp.response);
              }
            })

            .then(res => { observer.next(res) }).catch(error => { observer.error(error) });
          } else {
            observer.error("Token exception");
          }
        });
      })
    );
  }

  // public postMultipartWithAuthHeader(url: any, params?: any, options: any = {}) {
  //   return this.getValidResponseData(new Observable((observer) => {
  //       this.session.getToken().then(token => {
  //         if (token) {
  //           let headers = { "Authorization": token, "Accept": "application/json", "Content-type": "multipart/form-data" };

  //           this.http.post(url, params, headers)
  //           .then(resp => options.responseType == 'text' ? resp.data : JSON.parse(resp.data))
  //           .then(res => { observer.next(res) }).catch(error => { observer.error(error) });
  //         } else {
  //           observer.error("Token exception");
  //         }
  //       });
  //     })
  //   );
  // }

  private getValidResponseData(wishedResponseData) {
    // if (this.hasNetworkConnection) {
      return wishedResponseData;
    // } else {
      // this.showConnectionError();
      // return Observable.fromPromise(new Promise(function(resolve, reject) {
      //   reject("deu ruim na conexão");
      // }));
    // }
  }

  private showConnectionError() {
     this.toastCtrl.create({
      message: 'Sem conexão com a internet!',
      duration: 3000,
      position: 'bottom'
    }).present();
  }

}
