import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Platform, ToastController } from 'ionic-angular';

import { Observable } from 'rxjs/Observable';

import { SessionProvider } from '../../providers/session/session';

import { Network } from '@ionic-native/network';

@Injectable()
export class HttpAngularProvider {

  private hasNetworkConnection: boolean = true;

  constructor(public platform: Platform,
              public http: HttpClient,
              public session: SessionProvider,
              private network: Network,
              public toastCtrl: ToastController) {
    
    this.platform.ready().then(() => {
      this.network.onConnect().subscribe(() => { this.hasNetworkConnection = true });
      this.network.onDisconnect().subscribe(() => { this.hasNetworkConnection = false });
    });
  }

  public get(url: any, params?: any, options: any = {}) {
    options.params = params;
    return this.getValidResponseData(this.http.get(url, options));
  }

  public getHtml(url: any, params?: any, options: any = {}) {
    options['headers'] = new HttpHeaders({
        'Accept': 'text/html, application/xhtml+xml, */*',
        'Content-Type': 'text/plain'
      });
    options['responseType'] = 'text';
    return this.getValidResponseData(this.http.get(url, options));
  }

  public post(url: any, params?: any, options: any = {}) {
    let httpHeaders = new HttpHeaders({
      // 'Content-Type' : 'application/x-www-form-urlencoded'
      'Content-Type' : 'application/json'
    });
    options['headers'] = httpHeaders;
    return this.getValidResponseData(this.http.post(url, params, options));
  }

  public getWithAuthHeader(url: any, params?: any, options: any = {}) {
    return this.getValidResponseData(new Observable((observer) => {
        this.session.getToken().then(token => {
          if (token) {
            options.headers = {'Authorization': token};
            options.params = params;

            this.http.get(url, options).subscribe(res => {
              observer.next(res);
            }, error => {
              observer.error(error);
            });
          } else {
            observer.error("Token exception");
          }
        });
      })
    );
  }

  public deleteWithAuthHeader(url: any, params?: any, options: any = {}) {
    return this.getValidResponseData(new Observable((observer) => {
        this.session.getToken().then(token => {
          if (token) {
            options.headers = {'Authorization': token};
            options.params = params;

            this.http.delete(url, options).subscribe(res => {
              observer.next(res);
            }, error => {
              observer.error(error);
            });
          } else {
            observer.error("Token exception");
          }
        });
      })
    );
  }

  public postWithAuthHeader(url: any, params?: any, options: any = {}) {
    return this.getValidResponseData(new Observable((observer) => {
        this.session.getToken().then(token => {
          if (token) {
            let httpHeaders = new HttpHeaders();
            httpHeaders = httpHeaders.append('Authorization', token);
            httpHeaders = httpHeaders.append('Accept', 'application/json');
            httpHeaders = httpHeaders.append('Content-Type', 'application/json');
            options['headers'] = httpHeaders;

            this.http.post(url, params, options).subscribe(res => {
              observer.next(res);
            }, error => {
              observer.error(error);
            });
          } else {
            observer.error("Token exception");
          }
        });
      })
    );
  }

  public putWithAuthHeader(url: any, params?: any, options: any = {}) {
    return this.getValidResponseData(new Observable((observer) => {
        this.session.getToken().then(token => {
          if (token) {
            let httpHeaders = new HttpHeaders();
            httpHeaders = httpHeaders.append('Authorization', token);
            httpHeaders = httpHeaders.append('Accept', 'application/json');
            httpHeaders = httpHeaders.append('Content-Type', 'application/json');
            options['headers'] = httpHeaders;

            this.http.put(url, params, options).subscribe(res => {
              observer.next(res);
            }, error => {
              observer.error(error);
            });
          } else {
            observer.error("Token exception");
          }
        });
      })
    );
  }

  public postMultipartWithAuthHeader(url: any, params?: any, options: any = {}, others?: any) {
    return this.getValidResponseData(new Observable((observer) => {
        this.session.getToken().then(token => {
          if (token) {
            let httpHeaders = new HttpHeaders();
            httpHeaders = httpHeaders.append('Authorization', token);
            
            options['headers'] = httpHeaders;

            this.http.post(url, params, options).subscribe(res => {
              observer.next(res);
            }, error => {
              observer.error(error);
            });
          } else {
            observer.error("Token exception");
          }
        });
      })
    );
  }

  private getValidResponseData(wishedResponseData) {
    if (!this.hasNetworkConnection) {
      this.showConnectionError();
      return new Observable(observer => { observer.error("deu ruim na conexão") });
    } else {
      return wishedResponseData;
    }
  }

  private showConnectionError() {
     this.toastCtrl.create({
      message: 'Sem conexão com a internet!',
      duration: 3000,
      position: 'bottom'
    }).present();
  }

}
