import { Component, Input } from '@angular/core';
import { Events } from 'ionic-angular';

@Component({
  selector: 'solicitacao-atendimento-item',
  templateUrl: 'solicitacao-atendimento-item.html'
})
export class SolicitacaoAtendimentoItemComponent {

  @Input() public id: string;
  @Input() public status: string;
  @Input() public summary: string;
  @Input() public type: string;
  @Input() public response: string;

  constructor(private events: Events) { }

  public goToDetails(event) {
    if (!this.isInvalidData()) {
      this.events.publish('page:open', 'DetalhesSolicitacoesPage', {
        id: this.id
      });
    }
  }

  private isInvalidData() {
    return (this.id == '');
  }

}
