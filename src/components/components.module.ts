import { NgModule } from '@angular/core';
import { NewsItemComponent } from './news-item/news-item';
import { IonicModule } from 'ionic-angular';
import { PipesModule } from '../pipes/pipes.module';
import { SearchNewsItemComponent } from './search-news-item/search-news-item';
import { SolicitacaoAtendimentoItemComponent } from './solicitacao-atendimento-item/solicitacao-atendimento-item';
import { AssembleiaItemComponent } from './assembleia-item/assembleia-item';
import { LogBookListComponent } from './log-book-list/log-book-list';
import { ListSalarioComponent } from './list-salario/list-salario';
import { ParceriaItemComponent } from './parceria-item/parceria-item';

@NgModule({
	declarations: [NewsItemComponent, SearchNewsItemComponent,
    SolicitacaoAtendimentoItemComponent,
    AssembleiaItemComponent,
    LogBookListComponent,
    ListSalarioComponent,
    ParceriaItemComponent],
	imports: [IonicModule, PipesModule],
	exports: [NewsItemComponent, SearchNewsItemComponent,
    SolicitacaoAtendimentoItemComponent,
    AssembleiaItemComponent,
    LogBookListComponent,
    ListSalarioComponent,
    ParceriaItemComponent]
})
export class ComponentsModule {}
