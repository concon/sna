import { Component, Input, AfterViewInit } from '@angular/core';
import { Events } from 'ionic-angular';
import { WageCalculationUnitModel } from '../../app/models/wage-calculation-unit-model';

@Component({
  selector: 'list-salario',
  templateUrl: 'list-salario.html'
})
export class ListSalarioComponent implements AfterViewInit {

  @Input() public wageCalculation: WageCalculationUnitModel;

  constructor(private events: Events) { }

  ngAfterViewInit() { }

  public goToDetails(event) {
    if (!this.isInvalidData()) {
      if (this.wageCalculation.type == "Planejada" ) {
        this.events.publish('page:open', 'CalculoSalarioPage', {
          plannedWageCalculation: this.wageCalculation
        });
      } else {
        this.events.publish('page:open', 'CalculoSalarioPage', {
          executedWageCalculation: this.wageCalculation
        });
      }

    }
  }

  private isInvalidData() {
    return (this.wageCalculation == null);
  }

}
