import { Component, Input, AfterViewInit } from '@angular/core';
import { Events } from 'ionic-angular';
import { LogbookModel } from '../../app/models/logbook-model';
import {m} from "@angular/core/src/render3";

@Component({
  selector: 'log-book-list',
  templateUrl: 'log-book-list.html'
})
export class LogBookListComponent implements AfterViewInit {

    @Input() public logbook: LogbookModel;

    constructor(private events: Events) { }

    public goToLogBookDetalhesPage(event) {
      if (!this.isInvalidData()) {
        this.events.publish('page:open', 'LogBookDetalhesPage', {
        logbookData: this.logbook
      });
      }
    }

    public format(log){

        var minutos = log.horasTotal / 60000
        var horas = Math.floor(minutos/60);
        var minutosResto = minutos - (horas*60);
        var horasFormated = "";
        var minutosFormated = "";

        if(horas < 10){
          horasFormated = "0"+horas;
        }else{
          horasFormated = horas.toString();
        }

        if(minutosResto < 10){
          minutosFormated = "0"+minutosResto;
        }else{
          minutosFormated = minutosResto.toString();
        }

        return horasFormated+":"+minutosFormated;
    }

    ngAfterViewInit() {

    }

    private isInvalidData() {
      return (this.logbook == undefined);
    }

    public getDate(dateArray) {
      if (dateArray) {
        var year = dateArray[0];
        var month = dateArray[1];
        var day = dateArray[2];

        return month + "/" + day + "/" + year;
      } else {
        return "";
      }
      // MM/dd/yyyy
    }

  }
