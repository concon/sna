import { Component, Input, AfterViewInit } from '@angular/core';
import { Events } from 'ionic-angular';

@Component({
  selector: 'search-news-item',
  templateUrl: 'search-news-item.html'
})
export class SearchNewsItemComponent implements AfterViewInit {

  @Input() public id: string;
  @Input() public category: string;
  @Input() public createdAt: string;
  @Input() public description: string;
  @Input() public title: string;
  @Input() public hasSocialShare: boolean;

  constructor(private events: Events) { }

  public goToDetails(event) {
    if (!this.isInvalidData()) {
      this.events.publish('page:open', 'NewDetailsPage', {
        id: this.id,
        hasSocialShare: this.hasSocialShare,
        shouldLoadData: true
      });
    }
  }

  ngAfterViewInit() {

  }

  private isInvalidData() {
    return (this.id == '');
  }

}
