import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'adjustExternalLinks',
  pure: false
})
export class AdjustExternalLinksPipe implements PipeTransform {
  
 	constructor() { }

  transform(content) {
  	content = String(content).replace(/(target=).(_blank|_parent|_top|_self|framename)./g, "");
    content = String(content).replace(/<a/g, "<a target='_blank'");
  	content = String(content).replace(/href="\//g, 'href="http://www.aeronautas.org.br\/');
  	content = String(content).replace(/href='\//g, "href='http://www.aeronautas.org.br\/");
  	content = String(content).replace(/src="images/g, 'src="http://www.aeronautas.org.br\/images');
  	content = String(content).replace(/src='images/g, "src='http://www.aeronautas.org.br\/images");
    content = String(content).replace(/href="/g, "onclick=\"(function(e){ window.open(e.target.href,'_system', 'location=yes'); e.preventDefault(); })(event)\" $&");
    //content = String(content).replace(/href="/g, "href=\"#\" window.open(\"");
    //content = String(content).replace(/window.open\("(.+?)"/g, "$&,'_system'\)")

    return content;
  }

}
