import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'emptySub',
})
export class EmptySubstitutePipe implements PipeTransform {

  transform(value: string, substitute) {
  	var formattedValue = value;
  	if (!value || value == undefined || value == 'null') {
  		formattedValue = substitute;
  	}
  	return formattedValue;
  }

}
