import { NgModule } from '@angular/core';
import { KeepHtmlPipe } from './keep-html/keep-html.pipe';
import { KeepContentPipe } from './keep-content/keep-content.pipe';
import { AdjustExternalLinksPipe } from './adjust-external-links/adjust-external-links';
import { KeepUrlPipe } from './keep-url/keep-url.pipe';
import { DurationTimePipe } from './duration-time/duration-time';
import { EmptySubstitutePipe } from './empty-substitute/empty-substitute';

@NgModule({
	declarations: [
		KeepHtmlPipe,
		KeepContentPipe,
		AdjustExternalLinksPipe,
		KeepUrlPipe,
		DurationTimePipe,
    EmptySubstitutePipe
  ],
	imports: [],
	exports: [
		KeepHtmlPipe,
		KeepContentPipe,
		AdjustExternalLinksPipe,
		KeepUrlPipe,
		DurationTimePipe,
    EmptySubstitutePipe
  ]
})
export class PipesModule {}
