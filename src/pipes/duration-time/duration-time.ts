import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'durationTime',
})
export class DurationTimePipe implements PipeTransform {

  transform(duration) {
    //seconds = Math.floor((duration / 1000) % 60),
    let minutes = Math.floor((duration / (1000 * 60)) % 60);
    let hours = Math.floor((duration / (1000 * 60 * 60)));
    let formattedTime = this.getTextTime(hours) + ":" + this.getTextTime(minutes) + " hrs";

    return formattedTime;
  }

  private getTextTime(time) {
  	return (time < 10 ? '0' : '') + time;
  }

}
