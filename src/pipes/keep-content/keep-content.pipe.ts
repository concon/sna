import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'keepContent',
  pure: false
})
export class KeepContentPipe implements PipeTransform {

	constructor() { }

  transform(content) {
    content = String(content).replace(/<br \/><br \/>/g, ' - ');
    content = String(content).replace(/<[^>]+>/gm, '');
    content = String(content).replace(/&nbsp;/g, ' ');
    content = String(content).trim();
    content = String(content).slice(0, 249);
    return this.capitalizeFirstLetterEachWordSplitBySpace(content);
  }

  private capitalizeFirstLetterEachWordSplitBySpace(string){
    var words = string.split(" ");
    var output = "";
    for (var i = 0 ; i < words.length; i ++){
      var lowerWord = words[i].toLowerCase();
      lowerWord = lowerWord.trim();
      var capitalizedWord = lowerWord.slice(0,1).toUpperCase() + lowerWord.slice(1);
      output += capitalizedWord;
      if (i != words.length-1){
        output+=" ";
      }
    }
    return output;
  }

}
